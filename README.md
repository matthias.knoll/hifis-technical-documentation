# Documentation for the HIFIS Cloud and AAI

This repository contains the documentation for all aspects of the HIFIS
cloud and Backbone AAI.

The documentation is deployed to and publically available at
**<https://hifis.net/doc/>**.

## Conventions

Markdown is the common format for the documentation. For the general syntax see <https://www.markdownguide.org/basic-syntax>.

## Contribute

You can contribute by logging in via Helmholtz AAI, create a branch from master, do your suggested changes and invite a project maintainer to review (and possibly merge) your changes.

## Local builds, External Resources

This documentation repository uses submodules.
For local builds, clone this repository *recursively*, with:
```
git clone --recursive git@codebase.helmholtz.cloud:hifis/overall/hifis-technical-documentation.git
```

If you have cloned the repository already, you might need to update the submodules with
```
git submodule sync --recursive
git submodule update --remote --recursive
```

This documentation uses the following plugins.
For local builds, you need to install them as necessary:

* Kroki diagrams: See [introduction here](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab).
    `pip install mkdocs-kroki-plugin`.
* The [redirect plugin](https://pypi.org/project/mkdocs-redirects/)
  `pip install mkdocs-redirects`
* The [material theme](https://github.com/squidfunk/mkdocs-material)
  `pip install mkdocs-material`

The repository is public readable and it is writable for Helmholtz AAI logged in persons.

### Containers

This repository contains support for development containers (`.devcontainer`), including spellchecking and grammar checking in your editor. You need to have `vscode`, `podman` and `podman-compose` installed on your system. After following the steps above for cloning the repository and initializing the submodules, open the directory in VS Code and click `Reopen in Container`. You’ll then get spellchecking for markdown files. To run the `mkdocs` server locally, run `pipenv shell` followed by `mkdocs serve` in VS Code’s integrated terminal. You can then access the local instance at http://127.0.0.1:8000/doc/

## Automatic Builds (CI)

* Master build review app (password required):
  <https://hifis-review-app.hzdr.de/review-apps/hifis-overall-hifis-technical-documentation/master/>
* Download latest master build:
  <https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/jobs/artifacts/master/download?job=build>
* Browse latest master build:
  <https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/jobs/artifacts/master/browse/public?job=build>
