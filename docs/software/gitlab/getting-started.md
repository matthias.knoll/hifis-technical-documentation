---
title: Getting Started
---

# Getting Started

The [Helmholtz Codebase Service, based on GitLab :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud)
can be used free-of-charge by anybody within Helmholtz and their partners.
It is hosted by HIFIS Software entirely on HZDR servers,
jointly created by [UFZ :fontawesome-solid-up-right-from-square:](https://www.ufz.de) and [HZDR :fontawesome-solid-up-right-from-square:](https://www.hzdr.de).
With the general availability of the Helmholtz Cloud,
the service will be available as a fully integrated Helmholtz Cloud Service
and will follow the given regulations.

!!! info "gitlab.hzdr.de became codebase.helmholtz.cloud"
    Since November 11th, 2022, Helmholtz Codebase is available under the new domain [**codebase.helmholtz.cloud**](https://codebase.helmholtz.cloud).
    The previous domain [gitlab.hzdr.de](https://gitlab.hzdr.de) will redirect to this new domain. Nothing else changes for the users.

## Sign-In

### Available Options

There are two ways to sign in to the Helmholtz coding platform.

* **Helmholtz ID:** This is the recommended login method for everyone except
  HZDR employees (also known as Helmholtz AAI).
!!! success "Please note"
    In addition to a variety of scientific and university institutions, Helmholtz ID also supports the social
    providers **Google**, **GitHub** and **ORCID**. 
* **HZDR LDAP:** :fontawesome-solid-triangle-exclamation: Only HZDR employees can use this method.

!!! info "Switching to Helmholtz ID from any other login method"
    Please open your GitLab [account settings :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/-/profile/account)
    and choose *"Connect Helmholtz ID"* inside the *Social Sign-in* section. After successful login with the Helmholtz
    ID, you can disconnect from GitHub by choosing *"Disconnect GitHub"* in the previously mentioned *Social Sign-in* section.

### Helmholtz vs. Non-Helmholtz Users

Collaboration is a key component in software development.
This is why it is most important to enable anybody to easily contribute
changes to projects regardless of their affiliation.
Nevertheless, it is clear that we need to pose certain differences with respect
to users that do not belong to a Helmholtz organization.

* **Helmholtz employees** with registered home institution in Helmholtz ID get full access to all components.
* **All other users** will become
  [external users :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/help/administration/external_users.md).
  This means they need to be invited to projects or groups and will have
  access according to the permissions of the
  [role :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/help/user/permissions.html#project-members-permissions){ .md-badge .md-badge--primary }
  you assign. They can only create projects in their own personal namespace,
  but cannot create groups.

## First Steps

Follow the GitLab [basic guides :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/help/user/index.md)
to learn how to get started with the platform.
For more in-depth questions consider contacting the
[HIFIS Software Consulting :fontawesome-solid-up-right-from-square:](https://hifis.net/services/software/consulting) team.

### Clone a Git Repository as a Helmholtz ID User

GitLab, like all other Helmholtz ID connected services,
does not recognize a password from users who log in via Helmholtz ID.
Therefore, you cannot use your center password when Git asks for the
credentials while running the `git clone` command.
You have two options to interact with internal or private projects via `git`.

#### Create a Personal Access Token

* Go to <https://codebase.helmholtz.cloud/-/profile/personal_access_tokens> and create a
personal access token.
* Use the scopes `read_repository` and `write_repository`.
* (Optionally, define an expiration date after which the token is considered
  to be invalid.)
* Click on `Create personal access token`.
* GitLab will display the token once at the top of the page.
* Copy the token and save it in the password manager of your choice.

When Git asks for the password you can now enter the Personal Access Token you
just created and interact with Git as usual.

??? note "Screenshot of the Personal Access Token Page"
    ![Screenshot of the Personal Access Token creation](screenshots/access_token.png)

!!! info
    More information is available in the
    [GitLab documentation :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/help/user/profile/personal_access_tokens.html).

#### Use SSH to Interact With Your Repositories

Setting up SSH once might be the more convenient option for interacting with
your repository.
In this case you do not need to supply your username and password each time.
For configuring your local Git installation to properly use SSH we invite you
to have a look into the GitLab documentation.
This guide will help you through the process of setting up Git and SSH.

The process looks like this:

1. [See if you have an existing SSH key pair. :fontawesome-solid-up-right-from-square:][existing-ssh]
2. [Generate an SSH keypair if not already present. :fontawesome-solid-up-right-from-square:][create-ssh]
3. [Add an SSH public key to your GitLab account. :fontawesome-solid-up-right-from-square:][add-ssh]
4. [Verify that you can connect. :fontawesome-solid-up-right-from-square:][verify-ssh]

!!! info
    * Setup Git and SSH: <https://codebase.helmholtz.cloud/help/user/ssh.md>
    * On Windows: Please additionally refer to
      <https://codebase.helmholtz.cloud/help/user/ssh.md#use-ssh-on-microsoft-windows>

[existing-ssh]: https://codebase.helmholtz.cloud/help/user/ssh.md#see-if-you-have-an-existing-ssh-key-pair
[create-ssh]: https://codebase.helmholtz.cloud/help/user/ssh.md#generate-an-ssh-key-pair
[add-ssh]: https://codebase.helmholtz.cloud/help/user/ssh.md#add-an-ssh-key-to-your-gitlab-account
[verify-ssh]: https://codebase.helmholtz.cloud/help/user/ssh.md#verify-that-you-can-connect
