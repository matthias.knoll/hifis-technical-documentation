---
title: Changelog
---

## 2024-03-12 - GitLab 16.9

Update GitLab to version 16.9

* [Request changes on merge requests](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#request-changes-on-merge-requests)
* [Improvements to the CI/CD variables user interfaced](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#improvements-to-the-cicd-variables-user-interface)
* [Rich text editor broader availability](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#rich-text-editor-broader-availability)
* All details are available in the [Release Post](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/)

## 2024-02-12 - GitLab 16.8

Update GitLab to version 16.8

* [Predefined variables for merge request description][16-8-predefined-vars]
* [View blame information directly in the file page][16-8-blame-view]
* All details are available in the [Release Post][16-8-release-post]

[16-8-predefined-vars]: https://about.gitlab.com/releases/2024/01/18/gitlab-16-8-released/#predefined-variables-for-merge-request-description
[16-8-blame-view]: https://about.gitlab.com/releases/2024/01/18/gitlab-16-8-released/#view-blame-information-directly-in-the-file-page
[16-8-release-post]: https://about.gitlab.com/releases/2024/01/18/gitlab-16-8-released/

## 2024-01-16 - GitLab 16.7

Update GitLab to version 16.7

* [Customize time format for display][16-7-custom-time]
* [CI/CD Catalog - Beta release][16-7-ci-catalog]
* All details are available in the [Release Post][16-7-release-post]

[16-7-custom-time]: https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#customize-time-format-for-display
[16-7-ci-catalog]: https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#cicd-catalog-beta-release
[16-7-release-post]: https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/

## 2023-12-08 - GitLab 16.6

Update GitLab to version 16.6

* [Minimal forking - only include the default branch][16-6-minimal-forking]
* [CI/CD components Beta release][16-6-ci-components]
* All details are available in the [Release Post][16-6-release-post]

[16-6-minimal-forking]: https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#minimal-forking-only-include-the-default-branch
[16-6-ci-components]: https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/#cicd-components-beta-release
[16-6-release-post]: https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/

## 2023-11-16 - GitLab 16.5

* Update GitLab to version 16.5
    * [Resolve an issue thread][16-5-issue-resolve]
    * [Export individual wiki pages as PDF][16-5-wiki-pdf]
    * All details are available in the [Release Post][16-5-release-post]

[16-5-issue-resolve]: https://about.gitlab.com/releases/2023/10/22/gitlab-16-5-released/#resolve-an-issue-thread
[16-5-wiki-pdf]: https://about.gitlab.com/releases/2023/10/22/gitlab-16-5-released/#export-individual-wiki-pages-as-pdf
[16-5-release-post]: https://about.gitlab.com/releases/2023/10/22/gitlab-16-5-released/

## 2023-11-01 - GitLab 16.4

* Update GitLab to version 16.4
    * [Users with the Maintainer role can view runner details][16-4-runner-details]
    * [Custom email address for Service Desk][16-4-service-desk]
    * All details are available in the [Release Post][16-4-release-post]

[16-4-runner-details]: https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/#users-with-the-maintainer-role-can-view-runner-details
[16-4-service-desk]: https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/#custom-email-address-for-service-desk
[16-4-release-post]: https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/

## 2023-09-01 - GitLab 16.3

* Update GitLab to version 16.3
    * [Use the `needs` keyword with parallel jobs][16-3-needs-parallel-jobs]
    * [New navigation has color themes available][16-3-color-themes]
    * All details are available in the [Release Post][16-3-release-post]

[16-3-needs-parallel-jobs]: https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/#use-the-needs-keyword-with-parallel-jobs
[16-3-color-themes]: https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/#new-navigation-has-color-themes-available
[16-3-release-post]: https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/

## 2023-08-03 - GitLab 16.2

* Update GitLab to version 16.2
    * [All new rich text editor experience][16-2-rich-text-editor]
    * [Track your machine learning model experiments][16-2-ml-experiments]
    * All details are available in the [Release Post][16-2-release-post]

[16-2-rich-text-editor]: https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#all-new-rich-text-editor-experience
[16-2-ml-experiments]: https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/#track-your-machine-learning-model-experiments
[16-2-release-post]: https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/

## 2023-08-02 - GitLab 16.1

* Update GitLab to version 16.1
    * [All new navigation experience][16-1-new-navigation]
    * [Manage job artifacts through the Artifacts page][16-1-job-artifacts]
    * All details are available in the [Release Post][16-1-release-post]

[16-1-new-navigation]: https://about.gitlab.com/releases/2023/06/22/gitlab-16-1-released/#all-new-navigation-experience
[16-1-job-artifacts]: https://about.gitlab.com/releases/2023/06/22/gitlab-16-1-released/#manage-job-artifacts-through-the-artifacts-page
[16-1-release-post]: https://about.gitlab.com/releases/2023/06/22/gitlab-16-1-released/

## 2023-07-27 - GitLab 16.0

* Update GitLab to version 16.0
    * [Update your fork from the GitLab UI][16-0-update-your-fork]
    * [New Web IDE experience now generally available][16-0-new-web-ide]
    * All details are available in the [Release Post][16-0-release-post]

[16-0-update-your-fork]: https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#update-your-fork-from-the-gitlab-ui
[16-0-new-web-ide]: https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#new-web-ide-experience-now-generally-available
[16-0-release-post]: https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/

## 2023-05-10 - GitLab 15.11

* Update GitLab to version 15.11
    * [README files for groups][15-11-group-readme-files]
    * [Define inputs for included CI/CD configuration][15-11-ci-includes]
    * All details are available in the [Release Post][15-11-release-post]

[15-11-group-readme-files]: https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#readme-files-for-groups
[15-11-ci-includes]: https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#define-inputs-for-included-cicd-configuration
[15-11-release-post]: https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/

## 2023-04-11 - GitLab 15.10

* Update GitLab to version 15.10
    * [See all branch-related settings together][15-10-all-branch-settings]
    * [Discover commits by their tag in commits list view][15-10-discover-commits]
    * All details are available in the [Release Post][15-10-release-post]

[15-10-all-branch-settings]: https://about.gitlab.com/releases/2023/03/22/gitlab-15-10-released/#see-all-branch-related-settings-together
[15-10-discover-commits]: https://about.gitlab.com/releases/2023/03/22/gitlab-15-10-released/#discover-commits-by-their-tag-in-commits-list-view
[15-10-release-post]: https://about.gitlab.com/releases/2023/03/22/gitlab-15-10-released/

## 2023-03-06 - GitLab 15.9

* Update GitLab to version 15.9
    * [Track important incident timestamps on the incident timeline][15-9-incident-timestamp]
    * [Remove character limitations in unexpanded (raw) masked variables][15-9-masked-variables]
    * All details are available in the [Release Post][15-9-release-post]

[15-9-incident-timestamp]: https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#track-important-incident-timestamps-on-the-incident-timeline
[15-9-masked-variables]: https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/#remove-character-limitations-in-unexpanded-raw-masked-variables
[15-9-release-post]: https://about.gitlab.com/releases/2023/02/22/gitlab-15-9-released/

## 2023-02-15 - GitLab 15.8

* Update GitLab to version 15.8
    * [Create To-Dos for project owners on access requests][15-8-todos-project-owners]
    * [Include expiring token’s name in email notification][15-8-email-notifications]
    * All details are available in the [Release Post][15-8-release-post]

[15-8-todos-project-owners]: https://about.gitlab.com/releases/2023/01/22/gitlab-15-8-released/#create-to-dos-for-project-owners-on-access-requests
[15-8-email-notifications]: https://about.gitlab.com/releases/2023/01/22/gitlab-15-8-released/#include-expiring-tokens-name-in-email-notification
[15-8-release-post]: https://about.gitlab.com/releases/2023/01/22/gitlab-15-8-released/

## 2023-01-10 - GitLab 15.7

* Update GitLab to version 15.7
    * [Introducing the GitLab CLI][15-7-gitlab-cli]
    * [Sign commits with your SSH key][15-7-sign-commits]
    * All details are available in the [Release Post][15-7-release-post]

[15-7-gitlab-cli]: https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/#introducing-the-gitlab-cli
[15-7-sign-commits]: https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/#sign-commits-with-your-ssh-key
[15-7-release-post]: https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/

## 2022-12-12 - GitLab 15.6

* Update GitLab to version 15.6
    * [CI/CD variable support in rules:exists configuration][15-6-exists-in-rules]
    * [Support for special characters in CI/CD variables][15-6-special-characters]
    * All details are available in the [Release Post][15-6-release-post]

[15-6-exists-in-rules]: https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/#cicd-variable-support-in-rulesexists-configuration
[15-6-special-characters]: https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/#support-for-special-characters-in-cicd-variables
[15-6-release-post]: https://about.gitlab.com/releases/2022/11/22/gitlab-15-6-released/

## 2022-11-21 - GitLab 15.5

* Update GitLab to version 15.5
    * [Autocomplete suggestions in the Content Editor][15-5-auto-completion]
    * [Email notification when two-factor OTP attempt is wrong][15-5-2fa-email]
    * All details are available in the [Release Post][15-5-release-post]

[15-5-auto-completion]: https://about.gitlab.com/releases/2022/10/22/gitlab-15-5-released/#autocomplete-suggestions-in-the-content-editor
[15-5-2fa-email]: https://about.gitlab.com/releases/2022/10/22/gitlab-15-5-released/#email-notification-when-two-factor-otp-attempt-is-wrong
[15-5-release-post]: https://about.gitlab.com/releases/2022/10/22/gitlab-15-5-released/

## 2022-10-06 - GitLab 15.4

* Update GitLab to version 15.4
     * [Getting started with GitLab Pages just got easier][15-4-pages]
     * [Sortable, filterable data-driven tables in Markdown][15-4-tables]
     * All details are available in the [Release Post][15-4-release-post]

[15-4-pages]: https://about.gitlab.com/releases/2022/09/22/gitlab-15-4-released/#getting-started-with-gitlab-pages-just-got-easier
[15-4-tables]: https://about.gitlab.com/releases/2022/09/22/gitlab-15-4-released/#sortable-filterable-data-driven-tables-in-markdown
[15-4-release-post]: https://about.gitlab.com/releases/2022/09/22/gitlab-15-4-released/

## 2022-09-08 - GitLab 15.3

* Update GitLab to version 15.3
    * [Create tasks in issues][15-3-tasks]
    * [Submit merge request review with summary comment][15-3-summary]
    * All details are available in the [Release Post][15-3-release-post]

[15-3-tasks]: https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#create-tasks-in-issues
[15-3-summary]: https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#submit-merge-request-review-with-summary-comment
[15-3-release-post]: https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/

## 2022-08-02 - GitLab 15.2

* Update GitLab to version 15.2
    * [Live preview diagrams in the wiki WYSIWYG editor][15-2-live-preview]
    * [Improved and faster file browsing and syntax highlighting][15-2-faster-syntax]
    * All details are available in the [Release Post][15-2-release-post]

[15-2-live-preview]: https://about.gitlab.com/releases/2022/07/22/gitlab-15-2-released/#live-preview-diagrams-in-the-wiki-wysiwyg-editor
[15-2-faster-syntax]: https://about.gitlab.com/releases/2022/07/22/gitlab-15-2-released/#improved-and-faster-file-browsing-and-syntax-highlighting
[15-2-release-post]: https://about.gitlab.com/releases/2022/07/22/gitlab-15-2-released

## 2022-07-08

* Update GitLab to version 15.1
    * [Rendered images in Python notebook MRs](https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#rendered-images-in-python-notebook-mrs)
    * [Block Git access protocols at group level](https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#block-git-access-protocols-at-group-level)
    * All details are available in the
    [Release Post](https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/)

## 2022-06-09

* Update GitLab to version 15.0
    * [Edit code blocks, links, and media inline in the WYSIWYG editor](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/#edit-code-blocks-links-and-media-inline-in-the-wysiwyg-editor)
    * [Internal Notes](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/#internal-notes)
    * [Container Scanning available in all tiers](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/#container-scanning-available-in-all-tiers)
    * All details are available in the
    [Release Post](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/)

## 2022-05-06

* Update GitLab to version 14.10
    * [Improved pipeline variables inheritance](https://about.gitlab.com/releases/2022/04/22/gitlab-14-10-released/#improved-pipeline-variables-inheritance)
    * [Expanded view of group runners](https://about.gitlab.com/releases/2022/04/22/gitlab-14-10-released/#expanded-view-of-group-runners)
    * All details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/04/22/gitlab-14-10-released/)

## 2022-03-29

* Update GitLab to version 14.9
    * All details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/03/22/gitlab-14-9-released/)

## 2022-03-01

*  Update GitLab to version 14.8
    * Add default issue and merge request
      [templates](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#add-default-issue-and-merge-request-templates-in-a-projects-repository)
      in a project’s repository
    * [Improve pipeline index page layout](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#improve-pipeline-index-page-layout)
    * [Latest Release badge for the project](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#latest-release-badge-for-the-project)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/)

## 2022-02-07

* Update GitLab to version 14.7
    * Create
      [group access tokens](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/#group-access-tokens)
      using the UI and API
    * Major
      [Gitleaks](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/#major-gitleaks-performance-improvements)
      performance improvements
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/)

## 2022-01-12

* Update GitLab to version 14.6
    * Copy code blocks in Markdown with a single
      [click](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/#copy-code-blocks-in-markdown-with-a-single-click)
    * Toggle between editing in raw Markdown and WYSIWYG when editing
      [wiki pages](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/#toggle-wiki-editors-seamlessly)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/)

## 2021-12-03

* Update GitLab to version 14.5
    * Introducing Infrastructure as Code (IaC)
      [security scanning](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#introducing-infrastructure-as-code-iac-security-scanning)
    * Cleaner diffs for
      [Jupyter Notebook files](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#cleaner-diffs-for-jupyter-notebook-files)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/)

## 2021-11-22

* Update GitLab to version 14.4
    * Remote Repositories for GitLab in 
      [Visual Studio Code](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#remote-repositories-for-gitlab-in-visual-studio-code)
    * Integrated 
      [error tracking](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#integrated-error-tracking-inside-gitlab-without-a-sentry-instance) 
      inside GitLab without a Sentry instance
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released)

## 2021-10-05

* Update GitLab to version 14.3
    *  Introduces further improvements in the
       [wiki editor](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#edit-a-tables-structure-visually-in-the-new-wiki-editor)
    * Allows to
      [use variables in other variables](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#use-variables-in-other-variables)
      in `gitlab-ci.yml`
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/)

## 2021-08-27

* Update GitLab to version 14.2
    * Introduces [stageless pipelines](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#stageless-pipelines).
    * Adds a [Markdown live preview](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#preview-markdown-live-while-editing)
      to the Web IDE and single file editor to reduce the need for context switches.
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/)

## 2021-08-09

* Update GitLab to version 14.1 -
  [Release Post](https://about.gitlab.com/releases/2021/07/22/gitlab-14-1-released/)
