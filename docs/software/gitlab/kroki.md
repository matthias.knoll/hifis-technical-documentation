---
title: Textual Diagrams - Kroki
---

# Create Diagrams - Kroki

Helmholtz Codebase is equipped with the ability to create diagrams from
textual descriptions.
This post will help you getting started with this new feature.

## What is it about and where can it be applied?

Gitlab is perfect for collaborative work on codes, scripts
and other shared files.
With this integration you can also create virtually all sorts of diagrams
and sketches directly in Gitlab, both allowing to quickly create plots,
as well as collaboratively build and maintain complex graphs.

All graphs are built from simple scripts directly in Gitlab using
a plugin named _Kroki_.
Such scripts can be used in any place on the Helmholtz-wide Gitlab
where Markdown is supported:

* Markdown documents inside repositories
* Comments
* Issues
* Merge requests
* Milestones
* Snippets (the snippet must be named with a .md extension)
* Wiki pages

[:fontawesome-regular-lightbulb: Click here for examples in GitLab](https://codebase.helmholtz.cloud/examples/diagrams-in-gitlab){ .md-button .md-button--primary }

## Tutorial

The blog post on
[Easy Diagram Creation in GitLab :fontawesome-solid-up-right-from-square:](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab.html)
is a useful resource for more in-depth information.

## Further Reading

* [Kroki example project in GitLab :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/examples/diagrams-in-gitlab)
* [Official Kroki website :fontawesome-solid-up-right-from-square:](https://kroki.io/)
* [Kroki GitHub project :fontawesome-solid-up-right-from-square:](https://github.com/yuzutech/kroki)
