---
title: Getting Started
---

# Getting Started

The 
[Helmholtz Mattermost Service](https://mattermost.hzdr.de)
can be used free-of-charge by anybody within Helmholtz and their partners.
It is hosted by HIFIS Software entirely on HZDR servers,
jointly created by 
[UFZ :fontawesome-solid-up-right-from-square:](https://www.ufz.de)
and
[HZDR :fontawesome-solid-up-right-from-square:](https://www.hzdr.de).
With the general availability of the Helmholtz Cloud,
the service will be available as a fully integrated Helmholtz Cloud Service
and will follow the given regulations.

## Report Abuse

Mattermost is a communication and collaboration platform for anyone within
Helmholtz. 
All users are encouraged to follow common communication rules and a Netiquette.
In short, let's all be kind to each other. 

If you feel that people abuse this service or do not follow a Netiquette
you can report this by writing us an email to
[support@hifis.net](mailto:support@hifis.net).

## Sign-Up

Signing-in into the Helmholtz chat platform is done via the
[Helmholtz Codebase Service](https://codebase.helmholtz.cloud).
There are multiple 
[options to sign-in](../gitlab/getting-started.md#available-options).

!!! success "How to log in via Helmholtz AAI?"
    Please read the following
    [how-to](https://hifis.net/aai/howto)
    explaining by example all necessary steps to log in into a HIFIS service via 
    [Helmholtz AAI](https://login.helmholtz.de).
    There is also further
    [documentation](../../helmholtz-aai/howto-aai.md)
    available that covers the technical parts of how to log in with Helmholtz
    AAI.

??? info "Why do I need to sign-in via GitLab?"
    Mattermost cannot directly be connected to the
    [Helmholtz AAI](https://login.helmholtz.de), 
    instead it uses the Helmholtz AAI connection functionality of the
    Helmholtz Codebase Service as a so-called infrastructure proxy
    (see corresponding
    [documentation](../../helmholtz-aai/concepts.md#central-and-distributed-software-stacks)
    for more details).

## Changing Name and Email

In case your name and email address changed this needs to be corrected in the
[Helmholtz Codebase Service](https://codebase.helmholtz.cloud)
and **not** in the
[Helmholtz Mattermost Service](https://mattermost.hzdr.de)
itself.
In order to make these changes, you need to open your profile via the
_Edit profile_-button in GitLab and adapt your name and email address.
These changes will be propagated to Mattermost automatically afterwards.

??? info "Why do I need to change specific attributes via GitLab?"
    Specific user attribute changes can not be made within Mattermost.
    For changes of first name, last name, username and email address
    GitLab is the authoritative source for these user attributes. 
    This is because GitLab Single Sign-On (SSO) is configured as the only
    authentication method in Mattermost.

## User Roles and Permissions

Mattermost offers a couple of
[user roles](https://docs.mattermost.com/messaging/managing-members.html#user-roles)
depending on the context the user is currently in.
In addition to the usual default role _Member_, users can be _Team Admins_,
for example.

### Team Admins and Enabling Bot Notifications

Users who create teams are immediately assigned as a Team Admin.
Duties and permissions associate with this role is described in the
[Getting Started Guide for _Team Admins_](team-admins.md#getting-started-for-team-admins).

Since you as a _Team Admin_ are responsible for managing all your team members,
you also need to add bots to your team so that bots can send notifications to
your team members.
Please read the respective section in the
[_Team Admins_ Guide](team-admins.md#plugins-bots-and-bot-notifications)
to get to know more about bots available to you.

## First Steps

### Global Navigation Menu

Mattermost offers a navigation menu in the top left corner to select the
Mattermost channels, boards or playbooks.

![Global Navigation](images/global-navigation.png)

### Enter Teams and Channels

First things you might want to do is to assign yourself to a team and channels
belonging to that team.
Enter a team by invitation of the team admin or by browsing public teams via
the plus-button in the teams side-bar on the left-hand side.

![Add Team](images/add-team.png)

Once you are member of a team you are assigned to channels _Town Square_ and
_Off-Topic_.

![Initial Channels](images/initial-channels.png)

You can browse all other public channels and enter them via the plus-button
above to the channel search field in the channel side-bar on the left-hand side.

![Add Channel](images/add-channel.png)

### Create a Team

Of course, you can also create new teams by pressing the plus-button of
the teams side-bar.

### Create a Channel

In order to create new channels you can press the plus-button of the channels
side-bar.

![Create Channel](images/create-channel.png)

### Invite Members

Once the team and channels are set up you can add members to your team and
channels.

![Invite Members](images/add-members.png)

### Mentions

Inside your channels you can refer to other users by the
[mentions](https://docs.mattermost.com/messaging/mentioning-teammates.html)
feature when writing messages.
Possible mentions are:

* _@username_: to address a single member,
* _@channel_ and _@all_: to address all members in the channel,
* _@here_: to address all online members in the channel.

### Pin Messages

Sometimes it is convenient to pin a messages if it is of relevance very often
and need to be looked up often.
In Mattermost you can 
[pin messages](https://docs.mattermost.com/messaging/pinning-messages.html)
by clicking the _More actions_-button of a message and choose _Pin to Channel_.

![Pin Post](images/pin-post.png)

These pinned messages can be inspected by clicking the _Pinned posts_-button
below the channel name at the top.

![Pinned Posts](images/pinned-posts.png)

### Save Posts

It is also possible to 
[save messages](https://docs.mattermost.com/messaging/saving-messages.html)
by pressing the _Save_-button of a message.

![Save Post](images/save-post.png)

It is then added to your list of _Saved posts_ which you can inspect by
clicking the _Saved posts_-button in the top-right corner.

![Saved Posts](images/saved-posts.png)

### Sharing Messages

In order to
[share messages](https://docs.mattermost.com/messaging/sharing-messages.html)
with other users you may enter the _More actions_-drop-down and select
_Copy Link_.

![Share Post](images/share-post.png)

The resulting URL in the clipboard may then be distributed among other users.

## Important Features

It is worth mentioning and introducing the following Mattermost features:

* Collapsed Reply Threads,
* Boards,
* Playbooks.

### Collapsed Reply Threads

[Collapsed Reply Threads](https://docs.mattermost.com/messaging/organizing-conversations.html)
need to be enabled by each user separately.
The feature toggle button can be found in the _Display_ sub-menu of the
_Settings_ menu that can be found in the top-right corner.

![Thread Settings](images/thread-settings.png)

Usually, you may hit the _Reply_-button next to the message when answering to a
message.
By doing so, a discussion thread panel opens on the right-hand side displaying
the whole conversation thread.
The usual list of messages in the central main panel is then collapsed only
showing the original post and the reply count.
An additional side-bar entry _Threads_ will be displayed in the left channels
side-bar which opens a view in which all your threads are listed and can be
displayed.

![Threads](images/threads.png)

You may want to follow messages and threads so that you get notifications on
any activity.
Use the toggle-button _Follow/Following_ to subscribe ot/unsubscribe from
these notifications.

![Follow Thread](images/follow-thread.png)

### Mattermost Boards

Another brand-new Mattermost feature is the _Kanban_
[boards](https://docs.mattermost.com/guides/boards.html)
feature to organise and manage tasks as well as processes and workflows.
Each channel may have multiple boards and each board is a collection of
cards describing tasks that need to go through a specific workflow
displayed as a series of columns/groups on the boards, e.g.
_ToDo_, _Doing_, _Done_.

![Boards Dashboard](images/boards-dashboard.png)

??? info "How do you use Mattermost Boards in your daily work?"
    Please give us feedback on your use-cases and how you use Mattermost Boards
    in your daily work. 
    Feel free to write us an email to
    [support@hifis.net](mailto:support@hifis.net)
    and tell us more about it.

#### Board View

The boards view is a table-like structure with groups as columns and cards
as rows.
You can move the cards around between groups which changes also the status of
the card referring to the group name it is currently assigned to.

![Board View](images/board-view.png)

#### Working with Boards and Cards

In this section we describe how to add boards, groups, cards, properties and
comments (please read the Mattermost documentation for more detailed
information on how to
[work with boards](https://github.com/mattermost/focalboard/blob/main/docs/work-with-board-views.md)
and how to 
[work with cards](https://github.com/mattermost/focalboard/blob/main/docs/work-with-cards.md)).

In order to add these items you can use the plus-buttons displayed at the
respective positions in the boards view.
Boards can be added by clicking the plus-button in the boards side-bar on the
left-hand side when a particular channel has been chosen beforehand.

![Add Board Button](images/add-board-button.png)

The dialog to create boards lets us select templates or create boards from
scratch.

![New Board](images/new-board.png)

The first column is reserved for cards with no status.
New groups representing the status of the cards can be added by clicking
the _Add a group_-button next to the last group on the board.

![Add Group](images/add-group.png)

After having defined groups we are ready to add cards to the board by clicking
the _New_-button at the bottom of each group.

![Add Card](images/add-card.png)

Now, cards can be moved around between groups and the status will be adapted
accordingly.

Properties of a card hold information specific to a particular card like
effort estimations.
They can be added by clicking the respective _Add a property_-button in the
edit-card view.

![Add Property](images/add-property.png)

Another feature that can be found in the edit-card view is the comments feature.
Comments have an owner, a creation date and time as well as a message.
With comments you can keep track of comments each channel member made during
handling the respective cards on a particular board.

#### Board and Card Templates

Instead of creating boards and cards from scratch you can customize
[board and card templates](https://github.com/mattermost/focalboard/blob/main/docs/get-started-with-board-templates.md).

There are a couple of pre-defined templates and an empty-board template:

* Meeting Notes,
* Personal Goals,
* Personal Tasks,
* Project Tasks,
* Roadmap,
* New Template.

The available templates are listed when creating new boards.

To create a new card template you can click the _New template_-button in the
drop-down menu at the top-right of the board.

![Add Card Template](images/add-card-template.png)

The card edit dialog is all empty at first.

![Edit Card Template](images/edit-card-template.png)

After editing, a button for each card template is displayed in the same 
drop-down menu on the top-right of the board.

### Mattermost Playbooks

[Mattermost Playbooks](https://docs.mattermost.com/guides/playbooks.html)
offer a way to configure re-usable checklists for repeatable, recurring
workflows and processes for each team.

![Playbook Dashboard](images/playbook-dashboard.png)

Playbooks can be created from pre-built playbooks as well as from scratch.
Pre-build playbooks are the following:

* Product Release,
* Customer Onboarding,
* Service Reliability Incident,
* Feature Lifecycle.

![Add Playbook](images/add-playbook.png)

Playbooks are started by running them.

![Run Playbook](images/run-playbook.png)

Playbook runs are then listed in the _Runs_ view.
When running a playbook a private channel is created in the respective team.

![Run Playbook Dialog](images/run-playbook-dialog.png)

The private channel is named according to the run name chosen when running
the playbook.

![Playbook Run Channel](images/playbook-run-channel.png)

This channel is then populated with basic information about the playbook run.

![Playbook Run Messages](images/playbook-run-messages.png)

In the run details of this respective team channel additional members can be
assigned to the  playbook run as well as to individual tasks, tasks can be
checked as done, status update messages can be posted manually, and slash
commands can be triggered manually.

![Playbook Run Information](images/playbook-run-information.png)

Finally, a playbook run can be marked as finished by clicking _Finish run_.

![Playbook Run Finish](images/playbook-run-finish.png)

#### Create Playbook

In the _Playbooks_ view you find templates to be used for a new playbook.
After choosing one you need to select the team for which you would like to
create a new playbook.

#### Checklists

Since playbooks are essentially checklists with powerful extra features, the
main functionality is to define checklists with tasks that need to be worked
through by assigned members.
If slash commands are added to tasks they can be triggered manually during a
playbook run in the team channel created for a playbook run.

![Playbook Checklist](images/playbook-checklist.png)

#### Templates

Templates in the _Templates_ tab in the _Playbooks_ view are text blocks that
are either displayed when running a playbook or when a status update is posted
in the channel of the playbook run.
The status update posts can be timed with an update timer.
This time lets you choose the interval in which you will be reminded to post
an additional status update message.

![Playbook Template](images/playbook-template.png)

#### Actions

The _Actions_ tab in the _Playbooks_ view can be used to automate actions like:

* Prompting to run the playbook when a user posts a message containing specific
keywords,
* inviting members to the channel, 
* assigning the run owner role to a member,
* broadcasting status update posts to other channels,
* sending a welcome message to new channel members,
* adding a channel to a sidebar category.

![Playbook Actions](images/playbook-action.png)
