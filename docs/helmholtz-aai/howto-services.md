# Service Registration

To register a service (or client) in the Helmholtz AAI, you need to provide some necessary general information and some protocol specific information.

!!! info "There is a testing instance!"
    Note that you can **test the registration of your service(s)** also on a testing/development instance.
    In that case, replace all of the following URLs by <https://login-dev.helmholtz.de>.

## Service Registration depending on protocol

### OAuth2/OIDC service

If you want to register an OAuth2/OIDC service, you can do this yourself:

1. Browse to the [Helmholtz AAI oauth home endpoint](https://login.helmholtz.de/oauthhome).
1. Click on “*No account? Sign up.*” in the upper right corner.
    - Make sure that you are not logged in, otherwise the link is not displayed.
1. In next menu select “*Oauth2/OIDC client Registration*” which is the first item.
1. Fill in all necessary information.
    - The username is the client ID and the password is the client secret.
    - The OAuth client name will be the name displayed at the user's home endpoint. This can be relevant when changing consent settings.
    - Use the Comments field to provide special needs, like public client for Single-Page-Application.
1. By submitting the request, the AAI administrators are informed about your request and will review it.
1. The client can not be used unless it was accepted by the AAI administrators.

### SAML service

Self registration of SAML services, however, is not possible.
You need to [contact us](mailto:support@hifis.net) and provide all of the information above.
The AAI team will prepare the integration and come back to you to finish the integration.

Information needed to register the service, please provide this in your mail:

* Service name
* Service description
* URL to data privacy statement
* Service administrative contact (list preferred)
* Service security contact (list preferred)
* Site security contact (your local CERT)
* Further protocol specific information (see subsection)
* Postal adress of the organisation, providing the service
* Entity ID
* Return URL
* Client Logo (Link to a png file)

## Policies and security

For policy reasons you MUST provide:

- **Service Security Contact**: this contact MUST be able to provide information
  about user logins to the security people in case of a security incident.
  This contact is invited to register at the Infrastructure Security Point described in the [Security Incident Response](security-response.md).
- **Privacy Policy**:  This is a legal requirement. Luckily we do have a
  [GDPR compliant PP template](policies/README.md) into which you only have to insert the
  specifics of your service.

Further, you MAY provide:

- A custom **Service Access Policy** where you can specify user requirements for accessing the service, such as supported VOs and assurance levels.
    - No specific format is required, however you may adopt the [template](policies/helmholtz-aai/04_VO-AUP-Template.odt) / [PDF version](policies/helmholtz-aai/04_VO-AUP-Template.pdf).
    - Please make sure you are following the [guidelines](assurance.md) when expressing requirements for assurance.
      If you need additional user attributes besides those [available](attributes.md#provided-attributes-from-the-helmholtz-aai), you may also specify them here, but it is up to you to negotiate any bilateral agreements that might be required.
    - It is up to you to ensure that the users of your service are aware of and agree to abide by any applicable service AUPs.
      Helmholtz AAI doesn't provide any means to assist with that.
      However, the upcoming [Helmholtz Cloud](https://helmholtz.cloud/services/) will provide additional functionality like this (and others),
      thus you may consider to [apply for onboarding](mailto:support@hifis.net) of your service to the Helmholtz Cloud.

Additionally, you need to accept and abide by the following policies:

- [Helmholtz AAI Top Level Policy](policies/helmholtz-aai/00_HIFIS-Top-Level-Policy.pdf)
- [Security Incident Response Procedure](policies/helmholtz-aai/02_HIFIS-SIRP.pdf)
- [SIRTFI](https://refeds.org/sirtfi)
- [Policy on the Processing of Personal Data](policies/helmholtz-aai/03_HIFIS-PPPD.pdf)


## Further information and endpoints

### The mandatory information about the Helmholtz AAI IdP are:

* Entity ID: <https://login.helmholtz.de/saml-idp/metadata>
* Metadata URL: <https://login.helmholtz.de/saml-idp/metadata>
* Authentication Request Message: <https://login.helmholtz.de/saml-idp/saml2idp-web>
* SLO: <https://login.helmholtz.de/saml-idp/SLO-WEB>


The mandatory URLs of the Helmholtz AAI are:

* OAuth home endpoint: <https://login.helmholtz.de/oauthhome>
* Issuer: <https://login.helmholtz.de/oauth2>
* Discovery URL: <https://login.helmholtz.de/oauth2/.well-known/openid-configuration>
* Authorization endpoint: <https://login.helmholtz.de/oauth2-as/oauth2-authz>
* Token endpoint: <https://login.helmholtz.de/oauth2/token>
* Introspection endpoint: <https://login.helmholtz.de/oauth2/introspect>
* Revocation endpoint: <https://login.helmholtz.de/oauth2/revoke>
* Userinfo endpoint: <https://login.helmholtz.de/oauth2/userinfo>

### Token revokation

The path `/revoke` can be used to revoke an access token which was previously issued. This endpoint is implemented according to the RFC 7009, with the following Unity specifics:

* The token_type_hint is a mandatory argument, must be always provided (in RFC it is optional). The allowed values are `access_token` and `refresh_token`.
* The endpoint access is authorized implicitly by providing a valid token to be revoked. The `client_id` must be always given.

Typical usage:
```
POST /.../revoke HTTP/1.1
Host: ...
Content-Type: application/x-www-form-urlencoded

token=45ghiukldjahdnhzdauz&client_id=oauth-client&token_type_hint=refresh_token
```

