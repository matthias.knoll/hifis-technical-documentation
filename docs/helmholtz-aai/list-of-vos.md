# List of registered Virtual Organisations (VO)
This document lists the Virtual Organisations (VO) registered in the Helmholtz AAI. Currently, VOs can represent working groups or Helmholtz centres.

VO memberships of a logged in user are transmitted by the Helmholtz AAI via the group claim as [documented here](namespaces-registry.md).

## VOs of Working groups
Click on the corresponding link if you want to apply for a membership in the respective VO. This will send a corresponding mail to the HIFIS helpdesk. 
Depending on the VO policies, you may need to provide a reason on why you would like to become member.

### Group VOs in [helmholtz.de namespace](namespaces-registry.md#urngeanthelmholtzde-registry)

The required minimum [Assurance](assurance.md) in all cases is [AARC Assam](https://aarc-community.org/guidelines/aarc-g021/) (approximately corresponding to DFN AAI Basic).

⏩ Scroll to the right to see more details. ⏩

| VO Name | group claim, prefix:<br>`urn:geant:helmholtz.de:group:` | Registered | Last Changed | Contact | Application link |
| - | - | - | - | - | - |
| 0-Pollution-Projekt | `0-Pollution-Projekt` | 15.05.2023 | 15.05.2023 | Christian Schmidt |  |
| Arbeitskreise | `Arbeitskreise` | 05.05.2021 | 30.11.2022 | Rafael Ignaczak | <a href="mailto:it@helmholtz.de">Apply</a> |
| AirShowerPhysics | `AirShowerPhysics` | 26.10.2021 | 26.10.2021 | Ralf Ulrich |  |
| Allianz SP Digitalitaet | `Allianz_SP_Digitalitaet` | 14.12.2023 | 14.12.2023 | Roland Bertelmann  |  |
| ATLHGF | `ATLHGF` | 03.11.2021 | 03.11.2021 | Peter van der Reest, Philipp Mavridis |  |
| B3D_NRW-cluster | `B3D_NRW-cluster` | 17.11.2022 | 17.11.2022 | Holger Stiele, Susanne Pfalzner |  |
| COCAP | `COCAP` | 21.01.2022 | 20.02.2023 | Maren Göhler, Lennart Schüler |  |
| DLR-BW-IS-Pilot | `DLR-BW-IS-Pilot` | 17.03.2021 | 17.03.2021 | Yorck Schneider-Kühnle, Peter Kadlec |  |
| DLR-nubes | `DLR-nubes` | 17.03.2021 | 17.03.2021 | Yorck Schneider-Kühnle, Peter Kadlec |  |
| DLR-Pilot | `DLR-Pilot` | 26.11.2020 | 26.11.2020 | Yorck Schneider-Kühnle, Peter Kadlec |  |
| EAJADE | `EAJADE` | 03.08.2023 | 03.08.2023 | Thomas Schoerner-Sadenius, Patrick Fuhrmann |  |
| EnKliMoSyS | `EnKliMoSyS` | 30.04.2024 | 30.04.2024 | Carsten Hoyer-Klick | |
| ErUM-Wave | `ErUM-Wave` | 15.11.2023 | 15.11.2023 | Michael Bussmann |  |
| EuU_ADVANCE | `EuU_ADVANCE` | 18.06.2021 | 18.06.2021 | Cornelia Sattler |  |
| EuU_SynCom | `EuU_SynCom` | 18.06.2021 | 31.08.2023 | Marie Heidenreich |  |
| FAIR_Data_Management | `FAIR_Data_Management` | 07.09.2023 | 07.09.2023 | Oliver Knodel |
| FKLIB | `FKLIB` | 10.11.2023 | 10.11.2023 | Charaf Cherkouk  |
| Geo-INQUIRE | `Geo-INQUIRE` | 30.11.2022 | 30.11.2022 | Robert Kornmesser, Mateus Litwin Prestes, Stefanie Weege, Angelo Strollo |  |
| HCDC | `HCDC` | 27.01.2021 | 27.01.2021 | Ulrike Kleeberg |  |
| Head_Office | `Head_Office` | 05.05.2021 | 30.11.2022 | Rafael Ignaczak | <a href="mailto:it@helmholtz.de">Apply</a> |
| HelmholtzDataHub | `HelmholtzDataHub` | 26.11.2020 | 26.11.2020 | Sabine Barthlott |  |
| Helmholtz_Klimaneutral | `Helmholtz_Klimaneutral` | 27.01.2021 | 27.01.2021 | Peter Jüstel |  |
| Helmholtz_Research_Software_Forum | `Helmholtz_Research_Software_Forum` | 04.06.2021 | 04.06.2021 | Ants Finke |  |
| Helmholtz-SW-Award | `Helmholtz-SW-Award` | 28.07.2023 | 28.07.2023 | Uwe Konrad |  <a href="mailto:u.konrad@hzdr.de">Apply</a> |
| hereon_dkn | `hereon_dkn` | 31.05.2022 | 31.05.2022 | Tina Krönert |  |
| hereon_ksr | `hereon_ksr` | 10.09.2021 | 10.09.2021 | Burkhardt Rockel, Philipp Sommer |  |
| hereon_MarEns | `hereon_MarEns` | 07.02.2022 | 07.02.2022 | Beate Geyer, Phillip Sommer |  |
| HGF_Ombudsnetwork | `HGF_Ombudsnetwork` | 29.11.2023 | 29.11.2023 | Martina Pohl, Karin Lochte |  |
| HIFIS | `HIFIS` | 26.11.2020 | 26.11.2020 | Annette Spicker, Uwe Jandt |  |
| HiRSE | `HiRSE` | 04.05.2023 | 04.05.2023 | Rene Caspart, Robert Speck |  |
| HIP | `HIP` | 10.03.2021 | 10.03.2021 | Patrick Fuhrmann |  |
| Hi-Acts | `Hi-Acts` | 29.11.2023 | 29.11.2023 | Christine Leue, Johannes Blum |  |
| HMC | `HMC` | 26.11.2020 | 26.11.2020 | Emanuel Söding |  |
| HZI-HIRI | `HZI-HIRI` | 26.11.2020 | 26.11.2020 | Michael Kütt |  |
| HZI-Limesurvey | `HZI-Limesurvey` | 10.09.2021 | 10.09.2021 | Hendrik Eggers |  |
| ImpactMonitor | `ImpactMonitor` | 05.01.2024 | 05.01.2024 | Marko Alder, Patrick Ratei |  |
| Inkubator_Digitalisierung_WP1 | `Inkubator_Digitalisierung_WP1` | 27.01.2021 | 27.01.2021 | Uwe Konrad, Ants Finke |  |
| Inkubator_Digitalisierung_WP4 | `Inkubator_Digitalisierung_WP4` | 28.01.2021 | 28.01.2021 | Volker Gülzow |  |
| Inkubator_contact_management | `Inkubator_contact_management` | 25.02.2021 | 25.01.2021 | Annette Spicker |  |
| IT-SecurityHGF | `IT-SecurityHGF` | 18.01.2023 | 18.01.2023 | Benny Bräuer, Mario Meier |  |
| KIT_SensorManagement | `KIT_SensorManagement` | 16.02.2023 | 16.02.2023 | Benjamin Ertl, Sabine Barthlott  |  |
| KODA_AG_MS_Alternativen | `KODA_AG_MS_Alternativen` | 05.03.2021 | 05.03.2021 | Ants Finke |  |
| KS-Management | `KS-Management` | 20.05.2021 | 20.05.2021 | Nikoleta Bellou |  |
| LEAPS | `LEAPS` | 30.01.2024 | 30.01.2024 |  Paul Millar, Annika Thies, Petra Ullmann |  |
| Melcaya-DKFZ | `Melcaya-DKFZ` | 26.07.2023 | 26.07.2023 | Tabea Bucher |  |
| MOSES | `MOSES` | 26.10.2021 | 26.10.2021 | Ute Weber, Claudia Schütze |  |
| Net4Cities | `Net4Cities` | 14.12.2023 | 14.12.2023 | Erika von Schneidemesser |  |
| NGI-DE | `NGI-DE` | 07.05.2023 | 07.05.2023 | Patrick Fuhrmann, Tim Wetzel |  |
| OPTIMA | `OPTIMA` | 12.12.2023 | 12.12.2023 | Michael Bussmann, Daniel Kotik |  |
| POF4_Matter_MT_DMA | `POF4_Matter_MT_DMA` | 17.05.2021 | 28.04.2023 | Guido Juckeland, Michael Bussmann, Volker Gülzow |  |
| POF4_Topic4_Management | `POF4_Topic4_Management` | 27.01.2021 | 27.01.2021 | Juliane Petersen, Marcus Lange |  |
| POF4_Topic6_Management | `POF4_Topic6_Management` | 20.04.2021 | 20.04.2021 | Beate Slaby |  |
| reimagine_arctic_research | `reimagine_arctic_research` | 19.03.2024 | 19.03.2024 | Nina Doering |  |
| RIANA project | `riana-project` | 08.05.2024 | 08.05.2024 | Tom Minniberger |  |
| ROCK-IT | `ROCK-IT` | 01.02.2023 | 27.11.2023 | Britta Höpfner, Guido Juckeland, Mehdi Mohammad Kazemi |  |
| SeaVision | `SeaVision` | 29.11.2023 | 29.11.2023 | Veit Dausmann |  |
| SeroHub | `SeroHub` | 26.11.2020 | 26.11.2020 | Stephan Glöckner |  |
| SmartPhase | `SmartPhase` | 18.09.2023 | 18.09.2023 | Johannes Hagemann, Jeffrey Kelling  |  |
| SimDataCC | `SimDataCC` | 14.08.2023 | 14.08.2023 | Franz Rhee, Patrick Fuhrmann |  |
| softmatterspace | `softmatterspace` | 04.12.2023 | 04.12.2023 | Thomas Voigtmann |  |
| SOLACE | `SOLACE`| 28.04.2023 | 28.04.2023 | Guido Juckeland, Michael Bussmann |  |
| SOOP | `SOOP`| 01.09.2023 | 01.09.2023 | Bianca Brüssow |  |
| SSF-Hydrology | `SSF-Hydrology` | 14.07.2023 | 14.07.2023 | Anne Hartmann,Theresa Blume |
| STARS-Digital-LEAPS | `STARS-Digital-LEAPS` | 14.09.2022 | 14.09.2021 | Matthias Neeb, Klaus Kiefer |  |
| Training | `Training` | 06.04.2022 | 06.04.2022 | Patrick Fuhrmann |  |
| UFZ-IT | `UFZ-IT` | 27.01.2021 | 27.01.2021 | Thomas Schnicke |  |
| UFZ-Timeseries-Management | `UFZ-Timeseries-Management` | 16.06.2023 | 16.06.2023 | Martin Abbrent, David Schäfer, Tobias Kuhnert  |  |
| VISA_collaboration | `VISA_collaboration` | 14.04.2023 | 14.04.2023 | Tim Wetzel |  |

### Group VOs in [h-df.de namespace](namespaces-registry.md#urngeanth-dfde-registry)

| VO Name | group claim, prefix:<br>`urn:geant:h-df.de:group:` | Registered | Last Changed | Contact | Required [Assurance](assurance.md) |
| - | - | - | - | - | - |
| hdf-cloud-JSC | `hdf-cloud-JSC` | 14.11.2018 | 14.11.2018 | Sander Apweiler | RAF Cappuccino |
| IMK-TRO-EWCC | `IMK-TRO-EWCC` | 15.01.2019 | 15.01.2019 | Sundermann | HDF-DFN (or RAF Cappuccino) |
| m-team | `m-team` | 11.02.2020 | 11.02.2020 | Marcus Hardt | RAF Cappuccino |
| wlcg-test | `wlcg-test` | 18.04.2018 | 18.04.2018 | Marcus Hardt | IGTF Dogwood |

---

## VOs representing Helmholtz centres

The VO of the centres and Helmholtz-members are managed automatically by authentication via the IdPs of the centres.

The required [Assurance](assurance.md) in all cases is RAF Cappuccino.

| VO Name           |  group claim, prefix:<br>`urn:geant:helmholtz.de:group:`      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
|  AWI    | `AWI`   | 22.07.2020 | 22.07.2020  |
|  CISPA  | `CISPA` | 22.07.2020 | 22.07.2020  |
|  DESY   | `DESY`  | 22.07.2020 | 22.07.2020  |
|  DKFZ   | `DKFZ`  | 22.07.2020 | 22.07.2020  |
|  DLR    | `DLR`   | 22.07.2020 | 22.07.2020  |
|  DZNE   | `DZNE`  | 22.07.2020 | 22.07.2020  |
|  FZJ    | `FZJ`   | 22.07.2020 | 22.07.2020  |
|  GEOMAR | `GEOMAR`| 22.07.2020 | 22.07.2020  |
|  GFZ    | `GFZ`   | 22.07.2020 | 22.07.2020  |
|  GSI    | `GSI`   | 22.07.2020 | 22.07.2020  |
|  hereon | `hereon`| 22.07.2020 | 13.04.2021  |
|  HMGU   | `HMGU`  | 22.07.2020 | 22.07.2020  |
|  HZB    | `HZB`   | 22.07.2020 | 22.07.2020  |
|  HZDR   | `HZDR`  | 22.07.2020 | 22.07.2020  |
|  HZI    | `HZI`   | 22.07.2020 | 22.07.2020  |
|  Helmholtz-GS | `Helmholtz-GS` | 22.07.2020 | 22.07.2020  |
|  KIT    | `KIT`   | 22.07.2020 | 22.07.2020  |
|  MDC    | `MDC`   | 22.07.2020 | 22.07.2020  |
|  UFZ    | `UFZ`   | 22.07.2020 | 22.07.2020  |

Members of any of these VOs are automatically also member of:

| VO Name           |  group claim, prefix:<br>`urn:geant:helmholtz.de:group:`      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
|  Helmholtz-member | `Helmholtz-member` | 22.07.2020 | 22.07.2020  |

---

## Other supported VOs

The Helmholtz AAI does support some other VOs as well.

| VO Name           |  group claim      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
| DAPHNE4NFDI       | `urn:geant:dfn.de:nfdi.de:daphne:group:DAPHNE4NFDI` | 16.05.2022 | 16.05.2022 |
| NFDI4Immuno       | `urn:geant:dfn.de:nfdi.de:4immuno:group:NFDI4Immuno` | 05.04.2024 | 05.04.2024 |
| PUNCH4NFDI        | `urn:geant:dfn.de:nfdi.de:punch:group:PUNCH4NFDI` | 05.08.2021 | 05.08.2021 |
