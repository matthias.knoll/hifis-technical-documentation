# Hints for Identity Providers (IdP)

## How to join Helmholtz AAI as an IdP

Helmholtz AAI supports all IdPs from [eduGAIN](https://edugain.org/).

In order to collaborate and join as an identity provider, please join your national federation (in Germany: [DFN AAI](https://www.aai.dfn.de/)) and participate eduGAIN.

The mandatory information about the attributes consumed by the Helmholtz AAI, besides the federation metadata, are documented [here in the attributes section](attributes.md).

## Requirements for IdPs to respond to Attribute Query

* [Basic requirements for IdPs](Attribute_Query/SAML2_Attribute_Queries_basic.md)
* Advanced requirements for IdPs (*to be done*)
* [Collection of configuration files technically needed for Attribute Query response in Shibboleth 4](Attribute_Query/shibboleth_4_technical.md)

## Questions?

If you have further questions, please contact <support@hifis.net>.
