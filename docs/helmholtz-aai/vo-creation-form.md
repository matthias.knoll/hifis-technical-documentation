# Helmholtz AAI VO Creation Form

## Agreement between VO manager and Helmholtz AAI

<!---->


This document establishes a connection between the virtual organization (VO) and those
services that support the VO. It needs to be signed by the person
in charge of the VO.

<insert name of the VO\>

On behalf of the VO I supply the following contacts and will
notify the [HIFIS Management](mailto:office@hifis.net)
timely in case of change.

-   VO Manager (CM):
    -   <Name of the VO manager\>
    -   <Postal Address\>
    -   <Email\>
    -   <Telephone\>

<!---->

-   VO Security Contact:
    -   <Name of the Security Contact\>
    -   <Postal Address\>
    -   <Email\>
    -   <Telephone\>

On behalf of the VO, the CM accepts that he/she has:

-   Read, accepted and extended the following templates:
    -   [Acceptable Use Policy Template (AUP-Template)](policies/helmholtz-aai/04_VO-AUP-Template.odt) / [PDF version](policies/helmholtz-aai/04_VO-AUP-Template.pdf)
    -   [Privacy Policy Template (PP-Template)](policies/helmholtz-aai/05_VO-PP-Template.odt) / [PDF version](policies/helmholtz-aai/05_VO-PP-Template.pdf)

<!---->

-   Read and accepted the following policies:
    -   [Helmholtz AAI Top Level Policy](policies/helmholtz-aai/00_HIFIS-Top-Level-Policy.pdf)
    -   [Membership Management Policy](policies/helmholtz-aai/01_HIFIS-VOMMP.pdf)
    -   [Security Incident Response Procedure](policies/helmholtz-aai/02_HIFIS-SIRP.pdf)
    -   [Policy on the Processing of Personal Data](policies/helmholtz-aai/03_HIFIS-PPPD.pdf)

<!---->

-   Accepted responsibility that users will abide by Top Level Policy and its resulting policies, too.



**I have read and accepted the listed requirements.**

<signature\>

<!---->

The whole set of policies can be [found here.](policies/README.md)
