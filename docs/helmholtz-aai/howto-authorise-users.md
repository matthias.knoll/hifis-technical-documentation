# Authorising users for access to a service

Helmholtz AAI allows multiple ways to manage authorisation of users and user groups,
as well as various ways of transmitting these information to services.

!!! warning
    All these methods can be combined, which can easily result in highly complex or even chaotic authorisation schemas if not planned well beforehand.
    If you as a service provider plan to establish a non-trivial authorisation schema to heterogenous user groups, please check the following methods carefully,
    and, if in doubt, [contact us](mailto:support@hifis.net?subject=[aai]) to discuss optimal ways of implementation.


### Project-specific user groups
Users can be organised in hierarchical (tree-topology) groups - dubbed "**Virtual Organisation (VO)**".
VOs allow to provide services fine-grained information on entitlements of users,
as well as distributing the work load of user management to multiple sub-group managers, if needed.

- [How to register a virtual organisation (VO)](howto-vos.md)
- [How to manage users in VOs (and sub-VOs)](howto-vo-management.md)
- [Examples and practices on how to implement complex **role management** with VOs](../service-integration/roles/README.md)

The information is transmitted via `group` claims.
This and the already registered VOs are [listed here](list-of-vos.md).

### Organisation-specific user groups
Organisation-specific groups (VOs) are currently supported for all Helmholtz Centres and Helmholtz as whole.
This allows to identify a user automatically as a member of a supported organisation,
if the respective organisation's IdP provides sufficient information.
The [list of currently supported organisations can be found here](list-of-vos.md#vos-representing-helmholtz-centres).
For example, any full member of a Helmholtz organisation can be identified via the claim `urn:geant:helmholtz.de:group:Helmholtz-member`.

### Organisation-hierarchy user groups
Organisation-hierarchy groups (VOs) are currently supported by some Helmholtz Centres.
This allows to identify a user automatically as a member of a supported unity within the organisation,
if the respective organisation's IdP provides sufficient information.
The [list of currently supported organisations can be found here](namespaces-registry.md#urngeanthelmholtzde-registry).

### Resource capabilities
If the user is not member of a specific VO but is allowed to use a service, this is provided in information on entitlements of users. This information is transmitted via the `res` claim. In most cases, the entitlement value is defined by the service, expressed by the user's organisation and forwarded by the Helmholtz AAI.

### Assurance

IdPs can provide elevated assurance on the identity of the logged-in user,
for example stating that a photo ID has been checked and/or that multi factor authentication is employed.
This is expressed by the user's organisation and forwarded within the Helmholtz AAI as
[assurance in corresponding claims as described here](assurance.md). If the organisation provides only
the assurance information but not the fulfilled profiles, the Helmholtz AAI adds the profile information.
