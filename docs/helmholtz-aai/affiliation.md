# Affiliation in Helmholtz AAI

## Affiliation information
The affiliation information has well defined values. Only those values are
used to transfer information between the different systems. The possible affiliation values are:
`member`,
`student`,
`faculty`,
`staff`,
`employee`,
`alum`,
`affiliate`,
`library-walk-in`.

## Affiliation in home organisation
The Helmholtz AAI consumes the affiliation in the home organisation of an 
user from remote identity providers. Since this is not a mandatory information, 
not all identity providers send the affiliation attribute (eduPersonScopedAffiliation).
For those users where the information are available, the information can
be requested from Helmholtz AAI as voPersonExternalAffiliation attribute.

## Affiliation provided via Helmholtz AAI
The Helmholtz AAI aggregates and unifies information about the affiliation of a user in
or outside the Helmholtz Association, based on the affiliation information from the remote identity provider.

- This information can be requested as eduPersonScopedAffiliation attribute. 
- Helmholtz AAI will only use `staff` or `employee` or `affiliate` values to express the affiliation of the users.
- According to the eduPersonScopedAffiliation definition, the Helmholtz AAI will release `member` 
additionally to the `staff` or `employee` value.

#### Mapping:
- If a user authenticates via a non Helmholtz Centre or Head Office identity provider, the 
affiliation is set to `affiliate`, independent of the role in the home organisation.
- If the users authenticates using an identity provider
of a Helmholtz Centre or Head Office: 
    - If the Centre provides the values `staff` or `employee`,
the Helmholtz AAI will map them to the same value under the Helmholtz AAI scope.
    - All other values will be mapped to `affiliate`.

![Sketch of affiliation mapping](images/affiliation_transfer.svg)
