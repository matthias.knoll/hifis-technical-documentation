# Helmholtz AAI

This is the technical and administrative documentation on Helmholtz AAI, built and maintained by [HIFIS](https://hifis.net).

!!! info
    For a high-level representation, have a look at the [Helmholtz AAI Homepage](https://hifis.net/aai) on hifis.net.

Here we describe all you need to know about 
[basic steps for end users and group managers](howto-aai.md),
[registering a Virtual Organisation (VO)](howto-vos.md), 
[managing groups in VOs](howto-vo-management.md),
[registering a service](howto-services.md),
[joining as an identity provider](howto-idps.md), or
[using the AAI capabilities to authorise users and user groups](howto-authorise-users.md) for fine-grained access to your service(s),
and more (see menu).

We further describe our [goals](#goals), [technology](concepts.md), and [policies](policies/README.md),
and give further organisational information (see menu).

## Goals

The goal of Helmholtz AAI is to enable stakeholders with a Helmholtz background to accomplish several tasks:

- Enable the participating Helmholtz Centres to provide services to
  well defined sets of federated users, based on solid authentication
  and authorisation.
- Enable Principal Investigators (VO Managers) at Helmholtz Centres to allocate
  resources on behalf of their group (VO) and to manage the authorisation
  for the members of their VOs.
- Enable global researchers to use services provided by Helmholtz
  Centres - given they are properly authorised and their identity is
  adequately understood.
- Align with European activities that focus around the 
  [European Open Science Cloud (EOSC)](https://open-science-cloud.ec.europa.eu/).
