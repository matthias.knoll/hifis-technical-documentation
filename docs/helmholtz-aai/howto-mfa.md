# Configuration of multi factor authentication (MFA) credential

## Introduction

The multi-factor authentication (MFA) enhances the trust of user accounts. In multi-factor authentication users authenticate in the normal way with their organisational or social-id account and an additional time-based one-time password (OTP). The normal login is the first factor, the OTP is the second factor and can be generated in several ways.

!!! info
    The MFA is on some endpoints optional and on others mandatory. This may change at some point in future. But even using MFA where it is optional will increase the security of your account. When you enabled MFA to your account, you must use it on all endpoints. You can not decide to use it only on specific endpoints.
    MFA is

    * **optional** at
        - endpoints where you log in to services.
        - the userhome endpoints.
    * **mandatory** at
        - administrative endpoint.
        - the oauth home endpoints.
        - the VO management endpoint (_since July 2023_).
        
## Enabling MFA

!!! info
    There are multiple options to generate the OTP in this manual we assume that you are using an app on your smartphone. There are different free apps available, that implement the OTP algorithm, e.g. FreeOTP[iOS, Android] , Authy[iOS, Android], Google Authenticator, KeeWeb, Duo Mobile and LastPass and more.

The management of MFA is done within the userhome endpoint [/home](https://login.helmholtz.de/home) of the Helmholtz AAI. There You need to switch to the Credentials tab.

![Userhome_Credentials_tab_(red box)](images/mfa_management/credentials_tab.png)

Enable the Checkbox '*Enable two step authentication if possible*' and click on '*Setup*' in the One time password for second factor authentication section.

![Enable_MFA](images/mfa_management/enable_mfa.png)

Scan the generated QR code with your OTP app, and enter the 6-digit numerical code in the field provided, which the app will show you. Then click '*Update*'

![Configure_OTP](images/mfa_management/configure_otp.png)

## Need help?

[**Contact us**](mailto:support@hifis.net?subject=[aai]) if you need help.
