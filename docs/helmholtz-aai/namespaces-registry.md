# Namespaces + Registry

## urn:geant:h-df.de Registry

-  `urn:geant:h-df.de:group` contains 
    * **Group claims**  according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002).  
      The full list of VOs/groups and further information can be [found here](list-of-vos.md#group-vos-in-h-dfde-namespace).

| URN                                   | Purpose                                                                        | Registered | Last Changed | Contact                                                   |
|---------------------------------------|--------------------------------------------------------------------------------|------------|--------------|-----------------------------------------------------------|
| `group`                               | Used for group membership information exchange<br>according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) | 18.04.2018 | 18.04.2018 | <a href="mailto:support@hifis.net?subject=[aai]">HIFIS Support</a> |

To request a new namespace, please [contact our registry](mailto:support@hifis.net?subject=[Request h-df.de namespace]) and provide your targeted namespace.

---  

## urn:geant:helmholtz.de Registry

- `urn:geant:helmholtz.de` contains 

    * **Group claims** according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002).  
      The full list of VOs/groups and further information can be [found here](list-of-vos.md#group-vos-in-helmholtzde-namespace).  
    * **Entitlements** according to  [AARC-G027](https://aarc-community.org/guidelines/aarc-g027).  
      The full list of entitlements is listed below:

`PREFIX=urn:geant:helmholtz.de:`

| URN                                   | Purpose                                                                        | Registered | Last Changed | Contact                                                   |
|---------------------------------------|--------------------------------------------------------------------------------|------------|--------------|-----------------------------------------------------------|
| `group`                               | Used for group membership information exchange<br>according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) | 22.07.2020 | 22.07.2020 | <a href="mailto:support@hifis.net?subject=[aai]">HIFIS Support</a> |
| `gfz:group`                           | Used for expressing institute/devision membership information exchange<br>according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) | 09.06.2022 | 09.06.2022 | <a href="mailto:idp-admin@gfz-potsdam.de?subject=[gfz_namespace]">GFZ IdP Support</a> |
| `hereon:group`                        | Used for expressing institute/devision membership information exchange<br>according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) | 13.04.2022 | 13.04.2022 | <a href="mailto:support@hifis.net?subject=[aai]">HIFIS Support</a> |
| `hzb`                        | Used for expressing institute/devision membership information exchange<br>according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) | 29.05.2024 | 29.05.2024 | <a href="mailto:idp-admins@helmholtz-berlin.de?subject=[hzb namespace]">HZB IdP Support</a> |
| `res`                               | Used for resource capabilities information exchange<br>according to [AARC-G0027](https://aarc-community.org/guidelines/aarc-g027) | 14.09.2022 | 14.09.2022 | <a href="mailto:support@hifis.net?subject=[aai]">HIFIS Support</a> |


To request a new namespace, please [contact our registry](mailto:support@hifis.net?subject=[Request helmholtz.de namespace]) and provide your targeted namespace.


---

## urn:geant:dfn.de:helmholtz.de Registry

*  `urn:geant:dfn.de:helmholtz.de:group` contains group claims according to [AARC-G002](https://aarc-community.org/guidelines/aarc-g002) and entitlements according to [AARC-G027](https://aarc-community.org/guidelines/aarc-g027)

To request a new namespace, please [contact our registry](mailto:support@hifis.net?subject=[Request dfn.de:helmholtz.de namespace]) and provide your targeted namespace.  

`PREFIX=urn:geant:dfn.de:helmholtz.de:`

| URN                                   | Purpose                              | Registered | Last Changed | Contact                  |                                                                                                                                                                                                                                                                                                                                                   
|---------------------------------------|--------------------------------------|------------|--------------|--------------------------|

