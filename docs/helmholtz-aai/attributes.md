# Attributes in Helmholtz AAI

## Consumed Attributes from identity providers (IdP)
The Helmholtz AAI consumes only a small set of attributes from identity providers.
The attributes, that are generally requested are those specified in the
[=> REFEDS Research and Scholarship entity category](https://refeds.org/category/research-and-scholarship).
If the identity provider releases those attributes, the login for users works out of the box.

In addition, the `assurance` of the user will be described, using the [=> REFEDS Assurance Framework](https://wiki.refeds.org/display/ASS/REFEDS+Assurance+Framework+ver+1.0).

#### REFEDS Research and Scholarship

Also called R and S, or R&S.

You should read the original link (it's not long). In short, R&S makes sure
that the identiy provider will release these attributes.

- Mandatory:
    - shared user identifier
        - `eduPersonPrincipalName` (if non-reassigned)
        - `eduPersonPrincipalName` + `eduPersonTargetedID`

    - person name:
        - `givenName` + `sn`

    - email address:
        - `email`

- Optional:
    - affiliation
        - `eduPersonScopedAffiliation`

---

## Provided Attributes from the Helmholtz AAI

Please note, that the service MUST only request attributes that are
necessary for running it. Anything in addition is a violation to the GDPR
and may have legal consequences. User will see which attributes are going
to be released to the service.

#### SAML:

| Name                       | Format                            |
|----------------------------|-----------------------------------|
| `common name`                | `urn:oid:2.5.4.3`                   |
| `email`                      | `urn:oid:0.9.2342.19200300.100.1.3` |
| `eduPersonUniqueId`          | `urn:oid:1.3.6.1.4.1.5923.1.1.1.13` |
| `eduPersonScopedAffiliation` | `urn:oid:1.3.6.1.4.1.5923.1.1.1.9`  |
| `eduPersonPrincipalName`     | `urn:oid:1.3.6.1.4.1.5923.1.1.1.6`  |
| `givenName`                  | `urn:oid:2.5.4.42`                  |
| `sn`                         | `urn:oid:2.5.4.4`                   |
| `eduPersonEntitlement`       | `urn:oid:1.3.6.1.4.1.5923.1.1.1.7`  |

#### OAuth2/OIDC:

| scope                          | Attribute                                                                |
|--------------------------------|--------------------------------------------------------------------------|
| `openid`                       |                                                                          |
| `email`                        | `email`, `email_verified`                                                |
| `profile`                      | `name`, `eduperson_entitlement`, `given_name`, `family_name`, `preferred_username` |
| `credentials`                  | `ssh_key`, `preferred_username`                                            |
| `eduperson_scoped_affiliation` | `eduperson_scoped_affiliation`                                           |
| `eduperson_entitlement`        | `eduperson_entitlement`                                                  |
| `eduperson_principal_name`     | `eduperson_principal_name`                                               |
| `eduperson_unique_id`          | `eduperson_unique_id`                                                    |
| `eduperson_assurance`          | `eduperson_assurance`                                                    |
| `display_name`                 | `display_name`                                                           |
| `sn`                           | `sn`                                                                     |
| `single-logout`                |                                                                          |

#### Unique identifier in Helmholtz AAI:

The Helmholtz identifier offers an unique identifier for every user, which
allows to connect the accounts of the user on different services. This is
released as `eduPersonUniqueId`
(`SAML: urn:oid:1.3.6.1.4.1.5923.1.1.1.13` / `OIDC: eduperson_unique_id`). This ID is
created by the Helmholtz AAI itself. The Helmholtz AAI does not reuse
some identifier from the centres Identity providers for several reasons:

- `Email` is no identifier because it is not unique across time and will be
  reused if a user left the organisation
- `eduPersonPrincipalName` is no good identifier because centres may use the
  email address as `eduPersonUniqueId` too.
- Sent identifiers by centres are not used because some centres does not
  sent persistent identifiers but transient identifiers. Those are only
  unique for user, identity provider and service provider. If a user logs
  into two different services the IDs, released to this services, are not
  equal.

In case of SAML the identifier is the first part of the `eduPersonUniqueId`. In case of the OAuth/OIDC the `eduPersonUniqueId` is sub(without dashes)@iss.

#### Group membership information:

The Helmholtz AAI releases group membership information according to
[AARC-G002 guideline](https://aarc-community.org/guidelines/aarc-g002).
The information is released as `eduPersonEntitlement` in the following
format:

`<NAMESPACE>:group:GROUPNAME[:SUBGROUPNAME]#<AUTHORITY>`

The first part, inclusive the namespace, is the unique name of the group
information. The authority displays which provider accounted the user to
the group. Group membership information may be granted by different
authorities. The user can review the group memberships of groups which are
managed within the Helmholtz AAI in their profile in the home endpoint. In
this case only the GROUPNAME is displayed. The created `eduPersonEntitlement`
values are released together with information, received during the login
with the centres identity provider.

#### Resource capability information:

The Helmholtz AAI releases resource capability information according to
[AARC-G027 guideline](https://aarc-community.org/guidelines/aarc-g027). The information is released as `eduPersonEntitlement`
in the following format:

`<NAMESPACE>:res:RESOURCENAME[:PERMISSION]#<AUTHORITY>`

The first part, including the namespace, is the unique name of the
resources where capabilities are granted. The authority displays which
provider granted the permission to use the resource. Resource capabilities
may be granted by different authorities. The created `eduPersonEntitlement`
values are released together with information, received during the login
with the centres identity provider.
