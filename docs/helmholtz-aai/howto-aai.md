# How to do the basic steps in Helmholtz AAI

In a decentralized, networked model of cloud services known as federated cloud, the federated Authentication and Authorization Infrastructure (Helmholtz AAI) plays a crucial role. Proper usage of this system requires a few key steps and some foundational knowledge to avoid common pitfalls.

See here how to do the most important steps:

- A) As a user: [**How to log in to cloud services**](#a-as-a-user-how-to-log-in-to-cloud-services)
- B) As a user being invited to a group: [**How to join a group (VO)**](#b-as-a-user-being-invited-to-a-group-how-to-join-a-group-vo)
- C) As a group leader/PI: [**How to create and manage a group (VO)**](#c-as-a-group-leaderpi-how-to-create-and-manage-a-group-vo)

In all cases: If anything breaks, check [FAQ](https://hifis.net/faq#heaai), or [contact us](mailto:support@hifis.net).

---

## A) As a user: How to log in to cloud services

1. Go to a service page, e.g. listed in [Cloud Portal](https://helmholtz.cloud/services)
1. Click on Log in Button.
    - **Never log in directly on the service page.**
    - Instead, search for "Helmholtz ID" / "Helmholtz AAI".
1. Search for your institution, or alternatively ORCID, Google, Github if that suits you more.
1. Log in with your normal username, password etc
1. If it's your first time, accept usage conditions, etc.; accept data transfers.
1. You should end up in the service and see your content.

---

## B) As a user being invited to a group: How to join a group (VO)

1. In order to join a group ("virtual organisation", VO), you should have received a mail from Helmholtz AAI with a link. That mail has been triggered by your group manager (step C below).
1. Click the link.
1. Log in as described above (section A).
    - _**Wait for invitation page to load.** This can take several minutes; don't close or reload the tab. We're working on it._
1. Accept VO Policies and enter the group.
1. _Optional:_ You can check your group membership(s) anytime at <https://login.helmholtz.de/home/>. After the previous steps, the desired group should appear here.
1. **To make the new group membership have effect**, e.g. gaining access to group's data and resources, **you need to log in anew to that service**. That is:
    - If you're logged in to the service already, log out.
    - If the service is in DESY (e.g. dCache), also [log out from DESY's Login portal Keycloak](https://keycloak.desy.de/auth/realms/production/protocol/openid-connect/logout).
    - Log in to the service again.
1. You should see the content belonging to your new group.

---

## C) As a group leader/PI: How to create and manage a group (VO)

1. For your group, you may need a so-called "virtual organisation" (VO), if you
    - want to manage multiple users, in hierarchical groups, for multiple services, and/or
    - want to include users outside of Helmholtz.
1. If so, apply for a new group [as described here](./howto-vos.md).
1. Once granted: invite your users, optionally manage subgroups and delegate responsibilities, [as described here](./howto-vo-management.md).
    - _**Wait for invitations to be sent.** This can take several minutes; don't close or reload the tab. We're working on it._
1. Point your users to [this page, step B](#b-as-a-user-being-invited-to-a-group-how-to-join-a-group-vo) for basic How-to.
