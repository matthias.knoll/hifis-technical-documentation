# Further information: Login workflow

To use a service connected to the Helmholtz AAI, you simply choose one of the offered
[Helmholtz Cloud Services](https://helmholtz.cloud/services/). You may also refer to the list of [technically connected services and further documentation](../cloud-services/README.md).

When logging in to a service, just search for “Helmholtz AAI” and use your home institute’s credentials.

Please [refer to our pictured tutorial on how to access Cloud Services via Helmholtz AAI](https://hifis.net/aai/howto) for more details.


#### VO membership needed for some services

Some services will require you to be member of a [Virtual
Organisation](concepts.md#the-virtual-organisation-vo-concept) (VO). To become member, you need to be invited/added by the PI (=Administrator) of the VO.  This [list of VOs](list-of-vos.md) may help to find the appropriate contact.

If your [Home-IdP](#identity-providers) is from a Helmholtz centre and connected to the Helmholtz AAI, you are automatically member of the VO representing your organisation. This allows you to automatically gain access to certain services that allow all users from your home organisation.

------------
## Policies

As a user, you must only accept and abide by the Acceptable Use Policies (AUP) presented to you, either:

- VO AUP: when joining a VO, or

- SAUP: when using a service for the first time.

In addition, each VO you join and service you use which processes your personal data is required to provide a Privacy Policy (PP) and make it available to you.

You can find more information about the policies in use in the Helmholtz AAI [here](policies/README.md).

------------
## Technical

### How to log in with Helmholtz AAI

When you click your service, you will be redirected to the Helmholtz AAI community
proxy, powered by Unity. For using Unity you have to choose your home Identity
Provider (see below) at which you log in. 

#### First time login

On the first use you may need to verify your email address. Just follow the
procedures, it will work. Note that you need to click on "remember choice" at
several places to make login work smoother in subsequent attempts.

During the registration the first part of your email address is stored as
preferred username. You can update it in the [home interface](https://login.helmholtz.de/home).


#### Identity Providers

The AAI proxy does not manage identities by itself. For identities it trusts so called
Identity Providers (IdPs). As a user you must have an account with one of the
supported Identity Providers. Different classes of Identity Providers exist,
with pros and cons:

- "Home"-IdP: this is typically provided by your research centre or university.
  This is the place to which you get redirected in order to log in to a service.

- Social IdP: "social" networks like google or github also provide
  identities. However, these do not verify user-passports and do not have a
  work contract with their users. The services accessible with such identities may
  be limited.
