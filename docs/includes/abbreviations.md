*[2FA]: Two-factor Authentication
*[AARC]: Authentication and Authorisation for Research and Collaboration initiative
*[AAI]: Authentication and Authorisation Infrastructure, implemented and maintained by HIFIS for Helmholtz ID
*[API]: Application programming interface
*[CI/CD]: Continuous Integration and Deployment
*[CI]: Continuous Integration
*[CD]: Continuous Deployment
*[Codebase]: Helmholtz Codebase: Helmholtz-wide Gitlab instance, hosted at HZDR
*[DFG]: Deutsche Forschungsgemeinschaft
*[DFN]: Deutsches Forschungsnetz
*[dIS]: dCache InfiniteSpace, storage service provideed by DESY
*[EGI]: European GRID Infrastructure
*[EOSC]: European Open Science Cloud
*[FTS]: File Transfer System
*[FTS3]: File Transfer System
*[Helmholtz ID]: Central service, allowing unified login and group management for Helmholtz users and beyond. Set up and maintained by HIFIS.
*[HIFIS]: Helmholtz Federated IT services
*[HPC]: High Performace Computing
*[HTC]: High Throughput Computing
*[IdP]: Identity Provider
*[IdPs]: Identity Providers
*[ITIL]: Information Technology Infrastructure Library
*[JSON]: JavaScript Object Notation
*[KPI]: Key Performance Indicator
*[LHC]: Large Hadron Collider
*[MFA]: Multi-Factor Authentication
*[NFDI]: Nationale Deutsche Forschungsdateninfrastruktur
*[OIDC]: OpenID Connect
*[OTP]: One-time password
*[Plony]: Helmholtz Cloud Service Database, based on Plone, hosted at HZB
*[REST]: Representational state transfer
*[SAML]: Security Assertion Markup Language
*[SIRTFI]: Security Incident Response Trust Framework for Federated Identity
*[SSO]: Single-sign on
*[Unity]: Unity IdM, the software stack currently used for Helmholtz ID / AAI community proxy, hosted at FZJ
*[Virtual Organisation]: Representation of collaboration group in Helmholtz ID / AAI
*[Virtual Organisations]: Representation of collaboration groups in Helmholtz ID / AAI
*[VO]: Virtual Organisation (representation of collaboration group in Helmholtz ID / AAI)
*[VOs]: Virtual Organisations (representation of collaboration groups in Helmholtz ID / AAI)
*[WLCG]: Worldwide LHC (Large Hadron Collider) Computing Grid
