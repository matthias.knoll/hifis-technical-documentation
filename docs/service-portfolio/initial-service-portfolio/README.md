# Documentation for the Helmholtz Cloud - Service Portfolio

This site describes the creation of the (initial) Service Portfolio for Helmholtz Cloud, including the following points:

* [How the service list was created](how-the-service-list-was-created.md)
* [How services are evaluated (service selection criteria)](how-services-are-evaluated.md)
* [How services are selected (service selection process)](how-services-are-selected.md)

