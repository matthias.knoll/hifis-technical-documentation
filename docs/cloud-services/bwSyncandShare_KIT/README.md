# bwSync&Share Documentation

The bwSync&Share service enables employees and students of Helmholtz institutions to store data on KIT storage systems in order to synchronize them between desktop computers and mobile devices. In addition, users can make their stored data accessible to other users by assigning appropriate access rights. In this context, access rights can also be granted to users who do not belong to the participating institution.

By integrating OnlyOffice, an online editor comparable to Google Docs, Office documents can be edited via the web interface even without installing an Office suite on the end device. In addition to synchronizing and sharing files, this also enables joint, simultaneous editing of files.

After a user has registered for this service, authentication and authorization for service use always takes place via the Helmholtz AAI and the federated login service FeLS of KIT. The service can be used cross-platform via web browser, additionally clients for the operating systems Windows, Linux, Mac OS, as well as mobile apps for iOS and Android are available.

## Conditions of use

Access to bwSync&Share for members of Helmholtz centers is regulated by membership in a center service VO. In order for members of a Helmholtz center to use the bwSync&Share service, a center service VO of the form `<center_abbreviation>_bwSyncShare` must exist in the Helmholtz AAI.

The [overview of the currently established virtual organizations (VOs) in the Helmholtz AAI can be accessed here](../../helmholtz-aai/list-of-vos/).

An **exception** are the centers

 * Alfred Wegener Institute (**AWI**),
 * Helmholtz-Zentrum für Umweltforschung (**UFZ**) 
 * and Karlsruhe Institute of Technology (**KIT**)

which are already integrated into the service (for details on access and usage see [Usage of bwSync&Share](#usage-of-bwsyncshare)).

If no center service VO `<center_abbreviation>_bwSyncShare` exists yet, the following steps have to be taken to get access to bwSync&Share:

1. Establishment of a center service VO `<center_abbreviation>_bwSyncShare` by the using center.
   [How to request a center service VO](../../helmholtz-aai/howto-vos).
2. Designation of a contact person for the service at the using center.
3. Contacting the contact person via email to [bwsyncandshare@scc.kit.edu](mailto:bwsyncandshare@scc.kit.edu) to discuss with KIT the topics of
    * 1st level support and
    * usage licenses

### A center service VO for bwSync&Share already exists
To find out which virtual organizations (VOs) you are already a member of, log in at <https://login.helmholtz.de>. You will be presented with a start page with the item "Groups membership". This list should include the center service VO of the form `<center_abbreviation>_bwSyncShare`.

If this is not the case, please request membership in the center service VO `<center_abbreviation>_bwSyncShare`. To do so, click on the [Apply link in the corresponding line in the list of VOs](../../helmholtz-aai/list-of-vos/).

## Usage of bwSync&Share
1. Please click on the link ("Entrypoint") provided in the [Cloud Portal for the bwSync&Share](https://helmholtz.cloud/services/?filterProviderIDs=d55633b5-0a90-4593-8ecf-d094254aeb3c&serviceID=e7e66b7a-01bb-4405-b498-70f81591fe38) service. You will be redirected to the page of the Federated Login Service (FeLS) of KIT.
2. For members of the **AWI, UFZ** or **KIT**: Select the entry "AWI", "UFZ" or "KIT" as your home organization.
3. For members of all other Helmholtz Centers:
    1. Select the entry "Helmholtz AAI" in the list of home organizations. You will be redirected to a page to login to the Helmholtz AAI
    2. Select your home IdP and log in with your data
    3. Afterwards you will be automatically redirected back to the Helmholtz AAI and then to the service


## Data protection and data security
Communication between the end devices and the central storage systems is encrypted. The data is stored in encrypted form on KIT storage systems and thus remains within the German legal area. Data access is restricted to the user who initially stored the data and to other users authorized by the data owner by granting access rights. There is no backup in the form of a second copy.

The service is not approved by the operator for the storage of personal data.
When registering for the bwSync&Share service, the following user information is transmitted from the home institution to the service provider KIT in encrypted form and stored there:

* name and surname
* email address
* home institution
* Unique user ID (EPPN & persistent ID)
* Status of the user (student, employee, guest)

With your consent, you agree that the above data may be transferred to KIT for the purpose  of authentication and service provision. If you do not participate in the bwSync&Share service, this data will not be transferred to the bwSync&Share service. The provisions of the State Data Protection Act (LDSG) of the state of Baden-Württemberg and sector-specific data protection regulations (in particular German Telecommunication Act and German Telemedia Act) in the respectively applicable versions are observed.
The [»Regulations of Digital Information Processing and Communication« (I&C) at the Karlsruhe Institute of Technology](https://www.isb.kit.edu/downloads/Regelungen/201310_IuK_Ordnung_engl.pdf) apply.

## Operating time/service time/response time/availability
The systems of the service infrastructure are in regular operation around the clock. During the service time (weekdays from 09:00 to 17:00) the reaction time is four hours. The KIT strives for the highest possible service availability. Planned service interruptions (e.g. for maintenance work) are announced in advance with a period of five days.

## Costs
The use of bwSync&Share within the Helmholtz Cloud is free of charge for members of Helmholtz institutions. License costs may apply (see above "Conditions of use" <https://hifis.net/doc/cloud-services/bwSyncandShare_KIT/#conditions-of-use>).

## Storage
The amount of storage space available to a user is limited to 50 GByte.

## Tickets & Support
If you have any further questions, please contact the support team  of the institution to which you belong. There your request can be  recorded and if necessary forwarded to the support for  bwSync&Share. 

## More Links & Resources

* bwSync&Share Website: <https://bwsyncandshare.kit.edu>
* Hilfe & FAQ (deutsch): <https://help.bwsyncandshare.kit.edu/155.php>
* Help & FAQ (english): <https://help.bwsyncandshare.kit.edu/english/155.php>


