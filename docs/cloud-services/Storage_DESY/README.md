# InfiniteSpace / dCache

The DESY-hosted InfiniteSpace service, based on the dCache system, allows Helmholtz scientists and their collaboration partners to store scientific data and have access to it through different protocols across all Helmholtz Centres and associated institutes. It was one of the first services connected to the Helmholtz AAI. It is provided as part of the [Helmholtz Cloud](https://helmholtz.cloud).
See [dcache.org](https://dcache.org) and the [dCache GitHub project](https://github.com/dCache) for more details on the project.

When using this service, you agree to comply to the service's [Acceptable Use Policy](AUP.md).

!!! warning "Please be aware:"
    - This service is _not_ like Sync&Share. If you want to use an interactive, easy-to-use file storage and sharing service, please do use [Sync&Share](https://helmholtz.cloud/services/?filterKeywordIDs=860a8f9a-54aa-4868-9747-5df90d0e35e0) instead.
    - By design, files cannot be changed on-the-fly in dCache. Instead, every attempted change will result in deleting and rewriting that file.
    - dCache InfiniteStorage is meant for large-scale (>>1T) data storage and transfer in mostly automated data processing environments. It provides a graphical user interface, but only for _very_ basic functionality.
    - Basic command-line and scripting knowledge is a must-have to make reasonable use of this service. You need to have people with such basic knowledge in your user group. HIFIS can only provide very limited support.

## Getting Access for your Group

Usage of the service is not possible (and not reasonable) for single users.
Instead, groups that are coordinated by a Helmholtz member can request access to the storage and then invite (also non-Helmholtz) users to their so-called "virtual organisations (VO)".

[See here on how to request storage usage as a group manager](VO_application.md).

## Using and Managing

Users must belong to a Helmholtz-led collaboration group ("VO") that has been granted access to the service.

### Web frontend

Most conveniently, one can use the Web frontend and log in via Helmholtz ID:

- Go to [dCache InfiniteSpace](https://hifis-storage.desy.de/),
- select "Log in via OpenID-Connect Account",
- select "Helmholtz AAI(Keycloak)",
- select your home institution (or Google, Github, ORCID),
- identify yourself at your home institution.

See also our [illustrated tutorial on how such AAI login works in general](https://hifis.net/aai/howto).

### Other ways

- Via [rclone and oidc-agent](rclone_oidcagent.md)
- [API overview](https://hifis-storage.desy.de/api/v1/) (Swagger UI)
- Using the `gfal` tool kit
- _others_

## Usage Scenarios

- Direct storage and publication using the web frontend, rclone etc.
- Streaming of data through WAN using suitable tools and protocols
- Read-Only access to DESY compute ressources if applicable
- Large scale data transfer with HIFIS: [High-level intro to HTS](https://hifis.net/use-case/2021/10/20/use-case-hts)
- Automation of distributed data processing pipelines: See [demonstration on Helmholtz Imaging use case](https://hifis.net/use-case/2023/04/18/service-orchestration). As always: [Contact us!](mailto:support@hifis.net)
- _More to come._

## Have fun!

In case of problems or if you have suggestions, contact us at [support@hifis.net](mailto:support@hifis.net?subject=[hifis-storage]).
