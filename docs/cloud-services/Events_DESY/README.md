# Events Management, based on Indico

A Helmholtz-hosted Events Management service for everyone within Helmholtz and their partners, based on CERN's Indico.

For a full description of service please see the [entry in the Helmholtz Cloud Portal](https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b).

## Links and resources

* The canonical service description is found in the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b).
* [Direct link to service](https://events.hifis.net/)
* Tutorials:
    * [Basic tutorial for first steps](https://hifis.net/workshop-materials/tool-indico/)
    * [Indico software user documentation](https://learn.getindico.io/)
    * [Code repository](https://github.com/indico/indico)

## FAQ

### There are more Indico services around. Why this one?

This service, like all other [Helmholtz Cloud Services](https://helmholtz.cloud) is a community service dedicated to be used by Helmholtz centers, their users and collaboration partners.
With its open and federated login (via Helmholtz ID) it allows barrier-free cross-organisational collaboration.

### I already have an Indico account elsewhere. Can I log in with it?

No - Log in is accomplished via Helmholtz ID, which is unrelated.
[See here how this works](https://hifis.net/aai/howto).
In short: Click "Log in", select your organisation (or ORCID/Github/Google), and log in.
Done.

### I want to create an event in (sub-) category XY. But I have no write access. Help!

All categories shall be managed by the respective sub-communities, not centrally by HIFIS. So:

1. Check if the desired category already has managers (listed on the right). Contact them in the first place to let you create your event.
2. If there are no managers, or they do not respond, [contact us](mailto:support@hifis.net).
3. If you cannot find the appropriate category at all, contact us as well. We'll try to find a solution without blowing up the structure too much.

### So, HIFIS are the guys that provide the Indico, or ...?

HIFIS is much more: We are a Helmholtz-wide digital cooperation platform that creates, brokers and bundles digital services for Helmholtz and external partners, particularly for [scientific users](https://hifis.net/hifisfor/scientists). That is, HIFIS:

- builds the [Helmholtz Cloud](https://helmholtz.cloud), providing a large number of cloud services - including Indico;
- provides technical infrastructure, know-how (education, training, consulting) and community building for [good research software engineering](https://hifis.net/hifisfor/rse).

Check out the whole story, news and updates: <https://hifis.net>
