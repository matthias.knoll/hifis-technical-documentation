# Helmholtz Cloud Services

Currently, the Helmholtz Cloud Service Catalogue includes more than 25 high-quality services,
provided from Helmholtz Centres to members of the Helmholtz Association and their collaboration partners.
The [number of services are strongly increasing](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/artifact/plots/helmholtz-cloud-services/Service_Catalogue.pdf?job=plot_general).

- The service catalogue incl. usage conditions can be found at the [Helmholtz Cloud Portal](https://helmholtz.cloud).
- The underlying service information as well as upcoming services (service pipeline) can be found in the [Helmholtz Cloud Service Database Plony](https://plony.helmholtz.cloud/sc/public-service-portfolio).
- Further information can be found on our pages on:
    - [Process Framework for the Helmholtz Cloud Service Portfolio](../process-framework/Chapter-Overview.md)
    - [KPI Computation](../cloud-service-kpi/README.md)
    - [Service Integration / Cloud Architecture](../service-integration/README.md)
    - [Service specific documentation](service-docs-overview.md)

## All Helmholtz AAI connected services

Helmholtz Cloud Services resemble a subset of services connected to the Helmholtz AAI.
The complete lists of connected services to the Helmholtz AAI community proxy can be found as follows:

* [All Services connected to the production instance](https://login.helmholtz.de/unitygw/VAADIN/files/connected-services.html)
* [All Services connected to the development instance for integration testing](https://login-dev.helmholtz.de/unitygw/VAADIN/files/connected-services.html)
