# Technical documentation on some services

For a subset of services, please find additional technical and detailed information:

* [bwSync&Share](bwSyncandShare_KIT/README.md) (provided by KIT)
* [Codebase](Gitlab_HZDR/README.md) (provided by HZDR)
* [Events / Indico](Events_DESY/README.md) (provided by DESY)
* [InfiniteSpace / dCache](Storage_DESY/README.md) (provided by DESY)
* [Nubes](Nubes_HZB/README.md) (provided by HZB)
* [Rancher Managed Kubernetes](Rancher_DESY/README.md) (provided by DESY)

More details on services will be published when available.
