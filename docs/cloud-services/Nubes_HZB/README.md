# Nubes

Nubes is a Sync&Share platform that supports cooperation and collaboration across centers and user groups, inclusion of external users. 
Completely web-based, optional mobile client apps, any WebDAV clients. 

Nubes is provided by [HZB](http://www.helmholtz-berlin.de/).

### User Enablement
How to get access: To use the service, you need to be member of a Group which applies for service usage. You may apply for usage by contacting help@helmholtz-berlin.de.

Contact for user support: <help@helmholtz-berlin.de>.

## Technical Documentation 

* [AAI Integration](aai.md)
