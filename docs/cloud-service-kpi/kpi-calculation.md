# Helmholtz Cloud Service Operation KPI - detailed calculation

### Calculation
The **individual Service Operation KPI (raw KPI)** is calculated from the ratio between the starting value and the value at the time of reporting.   
The numeric calculation for the given KPI value are given in a publicly available [calculation repository](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

#### Calculation Period
Helmholtz Cloud Operation KPI is calculated in two ways:

1) **Overall**:   
from service start in [Helmholtz Cloud Portal](https://helmholtz.cloud/services/) to reporting date  
2) **Annual Reporting**:  
from beginning of current reporting period (beginning of year) to reporting date (end of year).

#### Definition of time points
##### Starting times
Due to the heterogeneity of Helmholtz Cloud Services onboarding time points, as well as the KPI reporting frequencies, multiple starting time points are defined to suit all use cases.

$$
T_{\rm fitstart\_overall}=\left(\begin{matrix}T_{\rm onboard} & \iff T_{\rm onboard} \geq T_{\rm logstart}\\T_{\rm logstart}&\rm else\end{matrix}\right.
$$

$$
T_{\rm reference\_overall}=\left(\begin{matrix}T_{\rm fitstart} & \iff f\left(T_{\rm fitstart}\right)\gg 0\\T_{\rm fitstart,manually\ defined}&\rm else\end{matrix}\right.
$$

with

- $T_{\rm onboard}$ denoting the time of [public availability of the service in the Helmholtz Cloud](https://plony.helmholtz.cloud/sc/public-service-portfolio).
- $T_{\rm logstart}$ denoting the time of the first data point in the [reported service KPI data](https://codebase.helmholtz.cloud/hifis/overall/kpi).

_Note:_ The potential definition of a needed $T_{\rm fitstart,manually defined}$ value is subject to the HIFIS Cloud Cluster, and is defined in the KPI computation repository.

##### End/Reporting time 
The reporting time point $T_{\rm report}$ is simply defined as the date-time of the end of the considered time frame (e.g., 2021-12-31 for the 2021 annual report).

#### Linear regression
For all available measurement series, a linear fit is calculated with linear regression (based on [gnuplot fit function](https://gnuplot.sourceforge.net/docs_4.2/node82.html), implementing nonlinear least-squares (NLLS) Marquardt-Levenberg algorithm). This yields for each **service _s_** and each reported **user or usage number _u_**:

- A slope:
  $m\left(s,u\right) \pm m_{\rm err}\left(s,u\right)$
- Intercept:
  $y_0\left(s,u\right) \pm y_{0,\rm err}\left(s,u\right)$

#### KPI Calculation

##### Raw KPI components 
The overall Helmholtz Cloud Operation KPI is composed out of an arbitrary number of **raw KPI** components and respective weights. This depicts the contribution of single services and single service usage information towards the overall development of the Helmholtz Cloud.

The raw KPI components _K_ are calculated independently for each _s_ and _u_, at the reporting time, based on linear fit calculated before:

$$
K\left(s,u,T_{\rm report}\right)=\frac{m\left(s,u\right)T_{\rm report}+y_0\left(s,u\right)}{m\left(s,u\right)T_{\rm reference\_overall}+y_0\left(s,u\right)}-1
$$

##### Weighting
If several measurement series are collected for a single service, each [raw KPI component is weighted](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/master/data/kpi-weights.csv) with a pre-defined weight $w_{s,u}$.
All services are of equal value; the weighting of all raw data of a service is 1 in total.  
If applicable and reasonable, the weighting of the raw usage data is 50% , as well as the weighting of the raw user data is 50%.

##### Side conditions
To avoid calculation from sparse data of too fresh services, raw KPI calculation is only performed if 

$$
T_{\rm report} \geq T_{\rm reference\_overall}+90{\ \rm d}
$$

#### Cumulated KPI
The **Helmholtz Cloud Service Operation KPI $K_o$** is calculated by sum of all weighted raw KPI components. It therefore increases with the number of services available and the increased usage and nuber of users of individual services.

$$
K_o\left(T_{\rm report}\right)=\sum_{s,u}{w_{s,u}K\left(s,u,T_{\rm report}\right)}
$$
