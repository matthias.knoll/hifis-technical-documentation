# Service Integration - Overview

Documentation for overarching technical components and considerations
within the Cloud Service Integration in HIFIS.

* [Specific documentation on some services](../cloud-services/service-docs-overview.md)
* [Helmholtz Cloud Agent](hca/)
* [Security Considerations](../cloud-services/security/aai-integration.md) on Service Integration

### Architecture

The currently envisioned overall architecture of the Helmholtz Cloud components is depicted below.
The architecture and several components are yet under development and subject to change.

![HCA_architecture](graphs/HIFIS_Cloud_Architecture.svg)
