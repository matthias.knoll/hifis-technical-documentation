# Definition of Service Specific Roles via AAI

The Helmholtz AAI allows multiple ways (and any combination thereof) on how to define service specific roles for a user in the context of using one or more specific service(s).
This is based on specific attributes delivered by the central community proxy and/or the home IdP of the user as [documented in the "Attributes Section" of Helmholtz AAI](../../helmholtz-aai/attributes.md#group-membership-information).

!!! info "What is this about? One Example..."
    For example, one signed-in user A, belonging to a specific project,
    may be allowed access a specific service only as normal user,
    while another user B belonging to the same group may be allowed to access the same service as a project administrator.
    After prior agreement with the service provider,
    the decision on these different user roles shall be made by, e.g., group managers or the home centre.
    After user log-in, the service only consumes the information on the user's role in this specific context and grants corresponding rights.

In the following, we document some current practices on role definition in Helmholtz AAI and Helmholtz Cloud.

## Variant 1: Group-based User Role, for one or more Service(s)

User roles can be specified and expressed via group claims over the Helmholtz AAI.

- In all cases, the service provider(s) define(s) and document a naming scheme for one or more specific sub-group(s).
- The roles can be specified at different entities, depending on use case.
  Further, the interpretation of these roles can be used for one specific service or for multiple services.
  Mixtures are possible and wanted for maximum flexibility and coverage of all relevant use cases:
    - 1a: Definition via a VO based group;
    - 1b: Definition via internal (institute) group structures;
    - 1c: Interpretation for multiple services with shared context.
- Have a look here for the [**specific implementation for Helmholtz DataHub**](roles-datahub.md).
- Note that this way of expressing user roles is a preliminary implementation, as long as the Helmholtz AAI software stack does not support explicit role assignments.
  Future adaptations are to be expected, i.e. expression via slightly modified claims.

## Variant 2: Centre/IdP-defined User Role

To define a specific role of a user belonging to an organization, specifically for a service:

- Service provider defines a `res` entitlement according to [documentation](../../helmholtz-aai/attributes.md#resource-capability-information).
- The definition shall be documented here for the specific service.
- Service provider is responsible to inform all necessary IdPs to transmit the respective entitlement,
  ideally referring to the documentation provided here.
- Helmholtz AAI will forward this entitlement to the service without modification.

## Variant 3: Group-independent User Role, for Multiple Services

To define a specific role of a user, independent of specific VO or group memberships,
it is planned that the Helmholtz AAI software stack will allow to define role definitions
that are orthogonal to VO or group memberships. Please
[contact us](mailto:support@hifis.net?subject=[aai])
if you need this feature for your use case.
