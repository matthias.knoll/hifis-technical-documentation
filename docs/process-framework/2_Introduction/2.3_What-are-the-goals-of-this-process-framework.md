# What are the goals of this Process Framework?

This Process Framework focuses on the explanation of the processes regarding the Service Portfolio Management for Helmholtz Cloud. It should give an overview of which processes exist, how they interconnect, which roles are involved in each process and what is included in each process steps.

This is explained with:

* Process visualizations
* Explanations of each process step and
* Process descriptions in this Process Framework

for [each process yet worked out](../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.0_Processes-regarding-the-Service-Portfolio.md).

This Process Framework can be used as a kind of handbook to understand how Service Portfolio Management for Helmholtz Cloud works.
