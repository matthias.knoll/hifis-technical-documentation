# The Service Catalogue and its structure

## Plony

The Service Catalogue is built in [Plony](https://plony.helmholtz.cloud/). The service object in Plony is created when saving the Application form for the first time. The service gets a persistent ID the, joins the Service Pipeline and service information is extended througout the Service Onboarding Process. As soon as the service's status switches to "Online", the service becomes part of the Service Catalogue and public service information is transferred to Cloud Portal.

When navigating to the Service Portfolio overview in Plony, all services are displayed, sorted by their status:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Service- Catalogue_Overview_Plony.png)

When clicking on a service, the corresponding Service Catalogue Entry is displayed. The Service Catalogue Entry includes all service information collected via the Application Form and the Service Canvas, sorted in tabs:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Service- Catalogue_Example-Service_Plony.png)

The Service Portfolio Manager can also have a look at the points gained through Weighting criteria during the Onboarding Process:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Service- Catalogue_Weighting-Criteria.png)

The last tab of the Service Catalogue entry enables the Service Portfolio Manager to manage the status of the service, e.g. if the service reached the offered capacities or is discontinued:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Service- Catalogue_Manage-Status.png)

## Helmholtz Cloud Portal

With the public service information transferred from Plony, the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/) displays all services in status "Online". Every service has a service card showing first details about the service, such as Service name, Keywords, Providing centre and a one-sentence description of the service's functionalities:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Cloud-Portal_Example-Servicecard.png)

When clicking on the service card, the full Service Description is displayed, giving all necessary information on the service's functionalities, how to get access, which limitations exists and where data is stored:

![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Cloud-Portal_Example-Service-Description.png)


## Further information

- [Current Services in the Service Portfolio](../../../../service-portfolio/service-portfolio-management/current-services-in-portfolio/).
- [Helmholtz Cloud Portal](https://helmholtz.cloud/services/), overview of all provided Services.
