# Process Framework for Helmholtz Cloud Service Portfolio
## Chapter Overview

* General Points
     * [Document history](1_General-points/1.0_Document-History.md)
     * [Validity of this document](1_General-points/1.1_Validity-of-this-document.md)
     * [Delimitations](1_General-points/1.2_Delimitations.md)
     * [List of abbreviations](1_General-points/1.3_List-of-Abbreviations.md)
* Introduction
     * [What is HIFIS?](2_Introduction/2.1_What-is-HIFIS.md)
     * [What are the goals of HIFIS Cloud?](2_Introduction/2.2_What-are-the-goals-of-HIFIS-Cloud.md)
     * [What are the goals of this Process Framework?](2_Introduction/2.3_What-are-the-goals-of-this-process-framework.md)
* Service Portfolio Management
     * [Service Portfolio Management in terms of ITIL – what’s part of it?](3_Service-Portfolio-Management/3.1_Service-Portfolio-Management-in-terms-of-ITIL.md)
     * [Service Type definitions](3_Service-Portfolio-Management/3.2_Service-Type-Definitions/3.2.0_Service-Type-Definitions.md)
     * [The Service Catalogue and its structure](3_Service-Portfolio-Management/3.3_The-Service-Catalogue-and-its-Structure/3.3.0_The-Service-Catalogue-and-its-Structure.md)
     * [Processes regarding the service portfolio](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.0_Processes-regarding-the-Service-Portfolio.md)
         * [Service Recruiting process](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.1_Service Recruiting Process.md)
         * [Onboarding process for new services](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services.md)
         * [Follow up Onboarding process](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.3_Follow Up Onboarding Process.md)
         * [Operation of services](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.4_Operation-of-Services.md)
         * [Offboarding process for services](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.5_Offboarding-Process.md)
         * [Review process for the Service Portfolio](3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.6_Review-Process-for-the-Service-Portfolio.md)
        
