---
hide:
  - navigation
  - toc
---

[![HIFIS-Logo](images/hifis_logo.svg)](https://hifis.net)
# Documentation for HIFIS Services, Helmholtz AAI, and Helmholtz Cloud

This repository summarizes all public technical and administrative documentation for the [Helmholtz Federated IT Services (HIFIS)](https://hifis.net) platform.

Please also have a [look at our roadmap](https://hifis.net/roadmap) for planned developments of HIFIS.

#### Contents

This documentation contains information on the

* [Authentication and Authorization Infrastructure (Helmholtz AAI)](helmholtz-aai/),
* HIFIS Backbone core services: [Transfer Service](core-services/fts-endpoint.md), [AAI proxy](core-services/aai-proxy.md), [Backbone network](core-services/backbone-network.md)
* Integration Details on [Helmholtz Cloud and Associated Services](cloud-services/)
* [Management of the Cloud Service Portfolio](service-portfolio/),
* [Details on the integration of specific cloud services](service-integration/),
* [Software Technology Services](software/), e.g. [Helmholtz Codebase](software/gitlab/getting-started.md).

#### Feedback
Please [**contact us**](https://hifis.net/contact.html) if you are missing any content or if you have general comments.

You are also invited to [contribute to this documentation via the corresponding repository](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation) (public readable, but AAI login required for write access).
