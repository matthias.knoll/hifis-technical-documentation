# HIFIS Cloud Portal - Architecture

## Scope

This document describes the general architecture of the Cloud Portal (CP) for the Helmholtz Federated IT Services (HIFIS).

The CP is a central part of the Helmholtz Cloud. It is the main entrypoint for users to find and access HIFIS / Helmholtz Cloud Services. In the position, the CP has to interact with almost all components of the system. Figure 1 shows an overview of the current system and the interaction with other systems.

![Login](images/Cloud_Portal_Architecture.png)

**Figure 1:** High-level system architecture

## Technology

### System Architecture HIFIS Cloud Portal

There are a couple of high-level components in the CP:

- Database
- Backend (Cerebrum)
- Frontend (Webapp)
- Helmholtz Cloud Agent (HCA) Service
- Availability Service


### Database

The CP uses a MongoDB server as database. MongoDB is a document-oriented database and as such is well suited as a backend for the CP. The main purpose of the CP is to present HIFIS services and their metadata. Most interactions with the database will only read data and use a variety of ad-hoc filters on flexible schemas which is well-supported in MongoDB.

Currently, there are 5 different collections:

- `marketService`: this is the main collection storing all service information, one document per service. It stores information like the service name, description, entrypoint, service provider and the used software.
- `image`: this collection stores base64 encoded PNG images that are linked from the marketService collection and show service and provider logos.
- `availability`: this collection stores availability information per services that is regularly updated by the availability service.
- `hcaRequest`: this collection stores the request that are sent to the Helmholtz Cloud Agents.
- `hcaResource`: this collection stores the resources that are created by HCA requests.

### Backend

The backend, a.k.a Cerebrum, provides access to the database using a REST API. It is written in Java and uses the Spring Boot framework.

### Frontend

The frontend is split in two parts: a single page application webapp written in HTML/Javascript and a server written in Java using Spring Boot. The server takes care of the authentication with the Helmholtz AAI and forwards the requests from the user to the backend. The webapp is the actual user interface with which the user interacts.

### HCA Service

The HCA service takes care of all interaction with the Helmholtz Cloud Agent. It is written in Python. It takes the requests that are sent by the user from the frontend and sends them to the corresponding RabbitMQ host. It handles the responses from the HCA and updates the resources in the database accordingly.

### Availability Service

The Availability service regularly checks if the services are still reachable and updates the corresponding DB collection. It is written in Python. If a service is not available and error message is stored. The availability information is displayed in the frontend using color-coded dots on the service cards.