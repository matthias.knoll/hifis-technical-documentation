# Service Interruption Announcements

## Purpose

If you want to inform your users on service downtimes, you can use HIFIS-managed channels to disseminate this information.
For example, HIFIS can make such announcements via banners in the [Helmholtz Cloud portal](https://helmholtz.cloud), Mattermost channels and others.

!!! Warning "Not for security incidents!"
    Be aware that such announcements are for planned issues. It is not intended to notify on security incidents.
    For security incidents, following the Helmholtz AAI [security incident response procedure](../helmholtz-aai/security-response.md) is mandatory.

## Procedure

Send a plain text mail to <cloud-notify@hifis.net> or, likewise, to the [HIFIS support](mailto:support@hifis.net?subject=[cloud-notify]):

- Service Name
- Providing Helmholtz Centre
- Date and Time when to set and when to remove the message
- Short Text (max 100 characters) for the main announcement
   - What is happening? E. g. downtime.
   - When? Date, time
   - How long? E. g. we expect a downtime of 5 minutes between 10:00 - 12:00.
- _optional:_ Longer Text (max 250 characters) for more detailed announcement. This will only be displayed in channels that allow lengthy messages (e.g., chat)
- _optional_: Define specific channels where to put the announcement. Otherwise, the [default channels](#default-channels) are used.

Template:

```
Start of Announcement:
YYYY-MM-DD hh:mm

End of Announcement:
YYYY-MM-DD hh:mm

Short text / One-Liner (What, When, How long), e.g.:
Service X will undergo planned maintenance and will not be accessible from 2023-05-31 18:00 CEST, for max. 2 hours.

Long text:
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea tak

Channels:
Portal, HIFIS website, ???
```

_Note:_ We might build an API for this when the need arises.

## Channels

### Default Channels

The following channels are used if not requested otherwise in the original mail:

- [Helmholtz Cloud Portal](https://helmholtz.cloud/),
- [Helmholtz Mattermost](https://mattermost.hzdr.de/), Town Square of ["HIFIS"](https://mattermost.hzdr.de/hifis/channels/town-square) and ["Helmholtz Platforms"](https://mattermost.hzdr.de/h-platforms/channels/town-square)
- Mailing list `service-announcement@hifis.net`.
    - This list is being populated by major user groups. 
    - If interested, [you can self-subscribe here](https://lists.desy.de/sympa/subscribe/hifis-service-announcement?previous_action=subscribe).

### Optional Channels

Depending on the type of message, if requested, we help you to broker notifications on other channels, like the hifis.net Homepage, Helmholtz Codebase, Chat Channels, Mailing Lists, other Helmholtz Cloud Services, etc.

## Feedback, Comments

If you have any questions, comments or requests, please [contact us](mailto:support@hifis.net).
